<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body>
    <div class="container-fluid" style="align-content: initial">
    <pre>
        Hi ${user} ,
        Greetings for the Day..!
        Your account is created by VIRTUAL WALLET Application Admin . 
        You can access application with below details.
    </pre>
    <pre>
        <span style="color: red">Application URL : <a href="${app_url}">VIRTUAL WALLET</a></span>
        <span>Username : ${email}</span>
        <span>Password : ${password}</span>
    </pre>
    <pre>
        For any doubts contact admin using +91 951 5047 912.
        Thanks & Regards,
    </pre>
</div>
</body>
</html>