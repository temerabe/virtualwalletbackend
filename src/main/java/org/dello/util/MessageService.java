package org.dello.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Service
public class MessageService {
	@Autowired
	private Environment env;

	public void sendMessage(String text, String to) {
		Twilio.init(env.getProperty("ACCOUNT_SID"), env.getProperty("AUTH_TOKEN"));
		Message message = Message.creator(new PhoneNumber("+918886318041"), // to
				new PhoneNumber(env.getProperty("MY_NOUMBER")), // from
				"مرحبا بك في المتداول الليبي ، هذه رسالة للتجريب فقط").create();
	}
}