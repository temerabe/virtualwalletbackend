package org.dello.util;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.model.Customer;
import org.dello.reg.customer.model.CustomerBalance;
import org.dello.reg.user.constant.EmailText;
import org.dello.reg.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * @author temerabe This is mail service class ,using to send mails to users
 */
@Service("emailService")
public class EmailSenderService {
	/*
	 * @Autowired() private JavaMailSender emailSender;
	 */
	@Autowired
	private Configuration freemarkerConfig;
	@Autowired
	private FreeMarkerConfigurer fr;
	@Autowired
	private SendGrid sendGrid;

	public EmailSenderService() {
		System.out.println("EmailSenderService.EmailSenderService()");
	}

	/**
	 * This method is used to send email registration success to users
	 * 
	 * @param user
	 * @throws MessagingException
	 * @throws IOException
	 * @throws TemplateException
	 */
	/*
	 * public void sendEmailToUser(User user) throws MessagingException,
	 * IOException, TemplateException { MimeMessage message =
	 * emailSender.createMimeMessage(); MimeMessageHelper helper = new
	 * MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
	 * StandardCharsets.UTF_8.name()); Map<String, Object> map = new HashMap<>();
	 * map.put("user", user.getFirstName()); map.put("password",
	 * user.getPassword()); map.put("email", user.getEmail()); map.put("app_url",
	 * EmailText.APP_URL_US + user.getId()); Template t =
	 * freemarkerConfig.getTemplate("usermail.ftl"); String html =
	 * FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
	 * helper.setTo(user.getEmail()); helper.setText(html, true);
	 * emailSender.send(message); }
	 */

	/*
	 * public void sendEmailToCustomer(CustomerEntity cust) throws
	 * MessagingException, IOException, TemplateException { MimeMessage message =
	 * emailSender.createMimeMessage(); MimeMessageHelper helper = new
	 * MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
	 * StandardCharsets.UTF_8.name()); Map<String, Object> map = new HashMap<>();
	 * map.put("user", cust.getFirstName() + " " + cust.getLastName());
	 * map.put("app_url", EmailText.APP_URL_CU + cust.getId()); Template t =
	 * freemarkerConfig.getTemplate("customermail.ftl"); String html =
	 * FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
	 * helper.setSubject("إكمال عملية التسجيل"); helper.setTo(cust.getEmail());
	 * helper.setText(html, true); emailSender.send(message); }
	 */

	public void sendEmailToCustomer(CustomerEntity cust) throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "تفعيل حسابك في المتداول الليبي";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("app_url", EmailText.APP_URL_CU + cust.getId());
		Template t = freemarkerConfig.getTemplate("customermail.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendWithdrawRequestEmail(CustomerEntity cust, String date, String txNo)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "طلب سحب من المتداول الليبي";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("image", EmailText.CUST_IMAGE + cust.getId());
		map.put("withdraw_date", date);
		map.put("tr_no", txNo);
		Template t = freemarkerConfig.getTemplate("withdraw_request.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendDepositApproved(CustomerEntity cust, String date, String txNo)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "طلب إيداع مبلغ مالي";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("deposit_date", date);
		map.put("tr_no", txNo);
		Template t = freemarkerConfig.getTemplate("deposit_approved.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendForgotPasswordEmail(CustomerEntity cust) throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "إعادة تعيين كلمة المرور";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("link", EmailText.APP_URL_RESET_PASSWORD + cust.getId());
		Template t = freemarkerConfig.getTemplate("forgot_password.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendCommonInterest(CustomerEntity cust, String date)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "نسبة الأرباح العامة";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("month", date);
		Template t = freemarkerConfig.getTemplate("common_interest_added.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendSpecialInterest(CustomerEntity cust, String date)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "نسبة أرباح خاصة";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("month", date);
		Template t = freemarkerConfig.getTemplate("special_interest_added.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendWithdrawRequestAccepted(CustomerEntity cust)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "قبول طلب السحب";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		Template t = freemarkerConfig.getTemplate("withdraw_request_befor.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendInvestMoneyRequest(CustomerEntity cust) throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "طلب تشغيل الأرباح";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		Template t = freemarkerConfig.getTemplate("invest_interest.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendInvestMoneyRequestComplete(CustomerEntity cust)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "الموافقة على طلب تشغيل الأرباح";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		Template t = freemarkerConfig.getTemplate("invest_complete.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendBalanceAdded(CustomerEntity cust, CustomerBalance balance)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "إضافة رصيد إلى حسابك";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("balance", balance.getAmount());
		Template t = freemarkerConfig.getTemplate("balance_added.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendDepositComplete(CustomerEntity cust, String balance)
			throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "إيداع مبلغ مالي";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		map.put("amount", balance);
		Template t = freemarkerConfig.getTemplate("deposit_complete.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void sendDepositRequest(CustomerEntity cust) throws MessagingException, IOException, TemplateException {
		Email from = new Email("idealstechnology@gmail.com");
		String subject = "طلب إيداع رصيد";
		Email to = new Email(cust.getEmail());
		Map<String, Object> map = new HashMap<>();
		map.put("user", cust.getFirstName() + " " + cust.getLastName());
		Template t = freemarkerConfig.getTemplate("deposit_request.ftl");
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, map);
		Content content = new Content("text/html", html);
		Mail mail = new Mail(from, subject, to, content);
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = this.sendGrid.api(request);
			sendGrid.api(request);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}