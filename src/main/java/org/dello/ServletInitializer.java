package org.dello;

import java.time.LocalDate;

import org.dello.reg.user.model.User;
import org.dello.reg.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(VirsualWallectAppApplication.class);
	}

}
