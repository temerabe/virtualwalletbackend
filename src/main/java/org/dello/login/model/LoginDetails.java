package org.dello.login.model;

import lombok.Data;

@Data
public class LoginDetails {

	private String email;
	private String password;

	public LoginDetails() {
		// no body
	}

	public LoginDetails(String email, String password) {
		this.email = email;
		this.password = password;
	}

}
