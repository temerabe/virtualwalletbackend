package org.dello.login.controller;

import org.dello.exceptions.UserNotFoundException;
import org.dello.login.model.LoginDetails;
import org.dello.login.service.LoginService;
import org.dello.reg.customer.entity.CustomerEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javassist.NotFoundException;

@RestController
public class LoginController {
	@Autowired
	private LoginService loginService;

	@PostMapping(value = "/customers/login", produces = "application/json")
	public Object checkCustomerLogin(@RequestBody LoginDetails login) {
		CustomerEntity checkCustomerLogin = loginService.checkCustomerLogin(login);
		if (checkCustomerLogin == null) {
			throw new UserNotFoundException("username = " + login.getEmail());
		} else {
			return checkCustomerLogin;
		}
	}

	@PostMapping("/customer/users")
	public CustomerEntity checkUsersLogin(@RequestBody LoginDetails login) {
		return loginService.checkCustomerLogin(login);
	}
}
