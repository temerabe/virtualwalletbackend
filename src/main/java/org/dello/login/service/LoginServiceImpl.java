package org.dello.login.service;

import java.util.List;
import java.util.Optional;

import org.dello.login.model.LoginDetails;
import org.dello.reg.admin.constant.AppConstant;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.repository.CustomerRepository;
import org.dello.reg.user.entity.UserEntity;
import org.dello.reg.user.repository.UserRepository;
import org.dello.util.PasswordEncryptionUsingAES;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private CustomerRepository custRepo;
	@Autowired
	private UserRepository userRepo;

	@Override
	public CustomerEntity checkCustomerLogin(LoginDetails login) {
		System.out.println(login);
		// encrypt the password
		String encrPass = null;
		try {
			encrPass = PasswordEncryptionUsingAES.encryptPassword(login.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
		Optional<CustomerEntity> entity = custRepo.doLogin(login.getEmail(), encrPass);
		if (entity.isPresent() && entity != null) {
			System.out.println(entity);
			return entity.get();
		}
		return null;
	}

	@Override
	public String activateUser(Integer userId) {
		Optional<UserEntity> findById = userRepo.findById(userId);
		findById.get().setStatus(AppConstant.ACCOUNT_STATUS_ACTIVE);
		UserEntity save = userRepo.save(findById.get());
		if (save != null) {
			return AppConstant.ACCOUNT_ACTIVATED;
		}
		return AppConstant.ACCOUNT_NOT_ACTIVATED;
	}

	@Override
	public String deActivateUser(Integer userId) {
		Optional<UserEntity> findById = userRepo.findById(userId);
		findById.get().setStatus(AppConstant.ACCOUNT_STATUS_INACTIVE);
		UserEntity save = userRepo.save(findById.get());
		if (save != null) {
			return AppConstant.ACCOUNT_DEACTIVATED;
		}
		return AppConstant.ACCOUNT_NOt_DEACTIVATED;
	}

	@Override
	public String activateCustomer(Integer custId) {
		Optional<CustomerEntity> findById = custRepo.findById(custId);
		findById.get().setAccountStatus(AppConstant.ACCOUNT_STATUS_ACTIVE);
		CustomerEntity save = custRepo.save(findById.get());
		if (save != null) {
			return AppConstant.ACCOUNT_ACTIVATED;
		}
		return AppConstant.ACCOUNT_NOT_ACTIVATED;
	}

	@Override
	public String deActivateCustomer(Integer custId) {
		Optional<CustomerEntity> findById = custRepo.findById(custId);
		findById.get().setAccountStatus(AppConstant.ACCOUNT_STATUS_INACTIVE);
		CustomerEntity save = custRepo.save(findById.get());
		if (save != null) {
			return AppConstant.ACCOUNT_DEACTIVATED;
		}
		return AppConstant.ACCOUNT_NOt_DEACTIVATED;
	}

	@Override
	public List<CustomerEntity> getCustomers() {
		return custRepo.findAll();
	}

	@Override
	public List<UserEntity> getUsers() {
		return userRepo.findAll();
	}
}