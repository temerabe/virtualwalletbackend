package org.dello.login.service;

import java.util.List;

import org.dello.login.model.LoginDetails;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.user.entity.UserEntity;

public interface LoginService {
	CustomerEntity checkCustomerLogin(LoginDetails login);

	String activateUser(Integer userId);

	String deActivateUser(Integer userId);

	String activateCustomer(Integer custId);

	String deActivateCustomer(Integer custId);

	List<CustomerEntity> getCustomers();

	List<UserEntity> getUsers();
}
