package org.dello.reg.admin.service;

import java.time.LocalDate;
import java.util.List;

import org.dello.reg.admin.entity.InterestReportEntity;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.InterestRateDetailsEntity;
import org.dello.reg.customer.entity.InvestInterestEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.dello.reg.customer.model.Customer;
import org.dello.reg.customer.model.CustomerBalance;
import org.springframework.stereotype.Service;

@Service
public interface AppService {
	String addBalance(CustomerBalance custBalance);

	String activateCustomer(Integer custId);

	String approveWithdraw(Integer id, String date);

	String approveTransfer(Integer id);

	String approveDeposit(Integer id, String date);

	String approveInvest(Integer id);

	List<WithdrawRequestEntity> viewPendingWithdrawRequest();

	List<InterestRateDetailsEntity> viewTemporarAddedInterest();

	List<DepositeRequestEntity> viewPendingDepositRequest();

	List<TransferEntity> viewPendingTransferRequest();

	List<InvestInterestEntity> viewPendingInvestRequests();

	List<CustomerEntity> viewCusPendingReg();

	List<CustomerEntity> getActiveCustomers();

	List<CustomerEntity> getCustomers();

	String addCommonInterest(Float interestRate, LocalDate date);

	String addSpecificInterest(Float interestRate, Long accountNo);

	Long getInActiveCustomerCount();

	Long getPenddingWithdrawCounts();

	Long getPenddingTransferCounts();

	Long getPenddingDepositCounts();

	Float getPenddingBalanceToWithdraw();

	Long getLastTransferCounts();

	Long getLastWithdrawCounts();

	String confirmInterest(Integer id);

	String updateCustomer(CustomerEntity cust);

	long getCurrentMonthInterestCount(LocalDate date);

	long getAllInterestCount();

	long getCurrentYearInterestCount(LocalDate date);

	List<InterestReportEntity> getMonthReport(LocalDate date);

	List<InterestReportEntity> getYearReport(LocalDate date);

	List<InterestReportEntity> getCustomerMonthlyInterest(long account, LocalDate type);

	List<InterestReportEntity> getCustomerYearlyInterest(long account, LocalDate type);

	List<InterestReportEntity> getCustomerAllInterest(long account, LocalDate type);

	long getCustomerMonthlyInterestCount(long account);

	long getCustomerYearlyInterestCount(long account);

	long getCustomertotalInterestCount(long account);
}