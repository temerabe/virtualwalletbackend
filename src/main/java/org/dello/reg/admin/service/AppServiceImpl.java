package org.dello.reg.admin.service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.mail.MessagingException;
import javax.swing.text.DateFormatter;

import org.dello.reg.admin.constant.AppConstant;
import org.dello.reg.admin.entity.InterestReportEntity;
import org.dello.reg.admin.repository.InterestReportRepository;
import org.dello.reg.customer.constant.CustomerConstant;
import org.dello.reg.customer.entity.CustomerBalanceEntity;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.InterestRateDetailsEntity;
import org.dello.reg.customer.entity.InvestInterestEntity;
import org.dello.reg.customer.entity.TransactionEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.dello.reg.customer.model.Customer;
import org.dello.reg.customer.model.CustomerBalance;
import org.dello.reg.customer.model.WithdrawRequest;
import org.dello.reg.customer.repository.CustomerBalanceRepository;
import org.dello.reg.customer.repository.CustomerRepository;
import org.dello.reg.customer.repository.DepositRepository;
import org.dello.reg.customer.repository.InterestRateDetailsRepository;
import org.dello.reg.customer.repository.InvestInterestRepository;
import org.dello.reg.customer.repository.TransactionRepository;
import org.dello.reg.customer.repository.TransferRepository;
import org.dello.reg.customer.repository.WithdrawRepository;
import org.dello.reg.customer.service.CustomerServiceImpl;
import org.dello.util.EmailSenderService;
import org.hibernate.service.spi.Wrapped;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.template.TemplateException;

@Service
public class AppServiceImpl implements AppService {
	@Autowired
	private CustomerRepository custRepo;
	@Autowired
	private WithdrawRepository wrRepo;
	@Autowired
	private TransferRepository trRepo;
	@Autowired
	private DepositRepository dpRep;
	@Autowired
	private TransactionRepository txRepo;
	@Autowired
	private EmailSenderService email;
	@Autowired
	private CustomerBalanceRepository custBalanceRep;
	@Autowired
	private InvestInterestRepository investRepo;
	@Autowired
	private InterestRateDetailsRepository intrRepo;
	@Autowired
	private InterestReportRepository repoRepo;

	@Override
	public String activateCustomer(Integer custId) {
		CustomerEntity cust = null;
		Optional<CustomerEntity> findById = custRepo.findById(custId);
		if (findById.isPresent()) {
			cust = findById.get();
		}
		cust.setAccountStatus(AppConstant.ACCOUNT_STATUS_ACTIVE);
		CustomerEntity save = custRepo.save(cust);
		if (save != null) {
			return AppConstant.ACCOUNT_ACTIVATED;
		} else {
			return AppConstant.ACCOUNT_NOT_ACTIVATED;
		}
	}

	@Override
	public String approveWithdraw(Integer id, String date) {
		String result = "فشلت العملية الرجاء عاود مرة أخرى";
		// get withdraw details by its id
		WithdrawRequestEntity withdrawRequestEntity = wrRepo.findById(id).get();
		withdrawRequestEntity.setStatus(CustomerConstant.TR_STATUS_APPROVED);
		withdrawRequestEntity.setApprovedAt(LocalDate.now());
		withdrawRequestEntity.setWithdrawWay(date);
		WithdrawRequestEntity save = wrRepo.save(withdrawRequestEntity);
		if (save != null) {
			// update customer balance
			CustomerEntity customerEntity = custRepo.findByAccountNo(save.getAccountNo()).get();
			// customerEntity.setBalanceToWithdraw(customerEntity.getBalanceToWithdraw() -
			// save.getAmount());
			// customerEntity.setSuspendedBalance(save.getAmount());
			// custRepo.save(customerEntity);
			// update the transaction status
			TransactionEntity findByProcessId = txRepo.findByProcessId(save.getId());
			findByProcessId.setStatus(CustomerConstant.TR_STATUS_APPROVED);
			findByProcessId.setApprovedAt(LocalDate.now());
			findByProcessId.setCustImage(customerEntity.getImage());
			findByProcessId.setCustId(customerEntity.getId());
			findByProcessId.setCustDelegImage(customerEntity.getDelegImage());
			findByProcessId.setWithdrawDate(date);
			if (customerEntity.getTotalBalance() != null && customerEntity.getTotalBalance() > 0) {
				findByProcessId.setCustomerBalance(customerEntity.getTotalBalance());
			} else {
				findByProcessId.setCustomerBalance(0F);
			}
			txRepo.save(findByProcessId);
			// send email contains the transaction number and date
			try {
				email.sendWithdrawRequestEmail(customerEntity, date, findByProcessId.getTransactionNo().toString());
			} catch (MessagingException | IOException | TemplateException e) {
				e.printStackTrace();
			}
			result = CustomerConstant.WITHDRAW_REVIEW_AR;
		} else {
			result = "فشلت العملية الرجاء عاود مرة أخرى";
		}
		return result;
	}

	@Override
	public List<WithdrawRequestEntity> viewPendingWithdrawRequest() {
		List<WithdrawRequestEntity> viewPendingWithdraRequest = wrRepo.viewPendingWithdraRequest();
		if (!viewPendingWithdraRequest.isEmpty() && viewPendingWithdraRequest != null) {
			System.out.println("it is not null");
			viewPendingWithdraRequest.forEach(t -> {
				System.out.println(t);
			});
			return viewPendingWithdraRequest;
		} else {
			System.out.println("it is null");
			return null;
		}
	}

	@Override
	public List<CustomerEntity> viewCusPendingReg() {
		// List<CustomerEntity> findAll = custRepo.findAll();
		List<CustomerEntity> penddinRegistration = custRepo.penddinRegistration();
		if (penddinRegistration.isEmpty()) {
			return null;
		} else {
			return penddinRegistration;
		}
	}

	@Override
	public List<DepositeRequestEntity> viewPendingDepositRequest() {
		Optional<List<DepositeRequestEntity>> viewPendingDepositRequest = dpRep.viewPendingDepositRequest();
		if (!viewPendingDepositRequest.get().isEmpty() && viewPendingDepositRequest != null) {
			return viewPendingDepositRequest.get();
		}
		return null;
	}

	@Override
	public List<TransferEntity> viewPendingTransferRequest() {
		Optional<List<TransferEntity>> viewPendingTransferRequest = trRepo.viewPendingTransferRequest();
		if (!viewPendingTransferRequest.get().isEmpty() && viewPendingTransferRequest != null) {
			return viewPendingTransferRequest.get();
		}
		return null;
	}

	@Override
	public List<CustomerEntity> getActiveCustomers() {
		return custRepo.getActiveCustomer();
	}

	@Override
	public List<CustomerEntity> getCustomers() {
		return custRepo.findAll();
	}

	@Override
	public String addBalance(CustomerBalance custBalance) {
		String result = "لم يتم إضافة الرصيد ، الرجاء إضافة رقم صحيح";
		if (custBalance.getAmount() != null && custBalance.getAmount() > 0 && custBalance.getAccountNo() != null) {
			CustomerEntity customerEntity = custRepo.findByAccountNo(custBalance.getAccountNo()).get();
			if (customerEntity != null) {
				if (custBalance.getAmount() != null && custBalance.getAmount() > 0
						&& custBalance.getAccountNo() != null) {
					if (customerEntity.getTotalBalance() != null) {
						if (custBalance.getWorkingDays() == 30) {
							customerEntity
									.setTotalBalance((customerEntity.getTotalBalance() + custBalance.getAmount()));
							// customerEntity.setAvailableBalance(customerEntity.getTotalBalance());
							if (customerEntity.getAvailableBalance() != null) {
								customerEntity.setAvailableBalance(
										(customerEntity.getAvailableBalance()) + custBalance.getAmount());
							} else {
								customerEntity.setAvailableBalance(custBalance.getAmount());
							}
						} else {
							customerEntity
									.setTotalBalance((customerEntity.getTotalBalance() + custBalance.getAmount()));
							// customerEntity.setAvailableBalance(customerEntity.getTotalBalance());
							if (customerEntity.getSuspendedBalance() != null) {
								customerEntity.setSuspendedBalance(
										(customerEntity.getSuspendedBalance()) + custBalance.getAmount());
							} else {
								customerEntity.setSuspendedBalance(custBalance.getAmount());
							}
						}

					} else {
						if (custBalance.getWorkingDays() == 30) {
							customerEntity.setTotalBalance(custBalance.getAmount());
							// customerEntity.setAvailableBalance(customerEntity.getTotalBalance());
							if (customerEntity.getAvailableBalance() != null) {
								customerEntity.setAvailableBalance(
										(customerEntity.getAvailableBalance()) + custBalance.getAmount());
							} else {
								customerEntity.setAvailableBalance(custBalance.getAmount());
							}
						} else {
							customerEntity.setTotalBalance(custBalance.getAmount());
							// customerEntity.setAvailableBalance(customerEntity.getTotalBalance());
							if (customerEntity.getSuspendedBalance() != null) {
								customerEntity.setSuspendedBalance(
										(customerEntity.getSuspendedBalance()) + custBalance.getAmount());
							} else {
								customerEntity.setSuspendedBalance(custBalance.getAmount());
							}
						}

						// result = "لم يتم إضافة الرصيد ، الرجاء إضافة رقم صحيح";
					}
					System.out.println("They set correctly");
					// set working days
					customerEntity.setWorkingDays(custBalance.getWorkingDays());
					customerEntity.setIsFromBegining(custBalance.getIsFromBegining());
					CustomerEntity save = custRepo.save(customerEntity);
					if (save != null) {
						// create customer balance record
						/*
						 * CustomerBalanceEntity balance = new CustomerBalanceEntity();
						 * balance.setAccountNo(save.getAccountNo());
						 * balance.setAmount(custBalance.getAmount());
						 * balance.setNotes(custBalance.getNotes());
						 * balance.setProccessType(custBalance.getProccessType());
						 * balance.setStartedAt(custBalance.getStartedAt());
						 * balance.setWorkingDays(custBalance.getWorkingDays());
						 * balance.setIsFromBegining(custBalance.getIsFromBegining());
						 * custBalanceRep.save(balance);
						 */
						// update customer's balance
						CustomerBalanceEntity customerBalance = new CustomerBalanceEntity();
						customerBalance.setAccountNo(customerEntity.getAccountNo());
						customerBalance.setAmount(custBalance.getAmount());
						customerBalance.setNotes(custBalance.getNotes());
						customerBalance.setProccessType(custBalance.getProccessType());
						customerBalance.setStartedAt(custBalance.getStartedAt());
						CustomerBalanceEntity save2 = custBalanceRep.save(customerBalance);
						// register transaction details
						TransactionEntity txEntity = new TransactionEntity();
						txEntity.setCreatedAt(LocalDate.now());
						txEntity.setAccountNo(save2.getAccountNo());
						txEntity.setTransactionNo(generateTransactionNo());
						txEntity.setTrType(CustomerConstant.TX_TYPE_DEPOSIT);
						txEntity.setAmount(customerBalance.getAmount());
						txEntity.setCustPassportNo(customerEntity.getPassportNo());
						txEntity.setCustDelegName(customerEntity.getDelegFullName());
						txEntity.setCustDelegPassportNo(customerEntity.getDelegPassport());
						txEntity.setCustFirstName(customerEntity.getFirstName());
						txEntity.setCustMiddleName(customerEntity.getMiddleName());
						txEntity.setCustLastName(customerEntity.getLastName());
						txEntity.setNotes(custBalance.getNotes());
						txEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
						txEntity.setProcessId(save2.getId());
						txEntity.setCustImage(customerEntity.getImage());
						txEntity.setCustId(customerEntity.getId());
						txEntity.setCustDelegImage(customerEntity.getDelegImage());
						txEntity.setStartedAt(custBalance.getStartedAt());
						if (save.getTotalBalance() != null && save.getTotalBalance() > 0) {
							txEntity.setCustomerBalance(save.getTotalBalance());
						} else {
							txEntity.setCustomerBalance(0F);
						}
						TransactionEntity trEntity = txRepo.save(txEntity);
						result = "تم إضافة الرصيد للمستثمر بنجاح";
						// send email
						try {
							email.sendBalanceAdded(save, custBalance);
						} catch (MessagingException | IOException | TemplateException e) {
							e.printStackTrace();
						}
					} else {
						result = "لم يتم إضافة الرصيد ، الرجاء إضافة رقم صحيح";
					}
				} else {
					System.out.println("they are null");
					result = "لم يتم إضافة الرصيد ، الرجاء إضافة رقم صحيح";
				}
			} else {
				result = "لم يتم إضافة الرصيد ، الرجاء إضافة رقم صحيح";
			}
		} else {
			result = "الحقول فارغة ، رجاء قم بملئها ";
		}
		return result;
	}

	@Override
	public String addCommonInterest(Float interestRate, LocalDate date) {
		String result = "الحقل فارغ";
		// check whether the variables empty or not
		if (interestRate != null && date != null && interestRate > 0) {
			// get all customer who has total balance
			List<CustomerEntity> customers = custRepo.findCustomerForInterest();
			if (customers != null && !customers.isEmpty()) {
				for (CustomerEntity t : customers) {
					// check whether customer has available balance or not
					if (t.getAvailableBalance() != null && t.getAvailableBalance() > 0
							&& t.getSuspendedBalance() != null && t.getSuspendedBalance() > 0) {
						// customer has both available balance and suspended balance
						System.out.println("available balance and suspended balance");
						// calculate interest for available balance
						Float interestForAvailableBalance = ((t.getAvailableBalance()) * (interestRate / 100));
						// calculate interest for suspended balance
						Float interestForSuspendedBalance = (((t.getSuspendedBalance()) * (interestRate / 100)) / 30)
								* (t.getWorkingDays());
						Float totalInterest = interestForAvailableBalance + interestForSuspendedBalance;
						InterestRateDetailsEntity interEntity = new InterestRateDetailsEntity();
						// fill in interest details
						interEntity.setAccountNo(t.getAccountNo());
						interEntity.setFirstName(t.getFirstName());
						interEntity.setMiddleName(t.getMiddleName());
						interEntity.setLastName(t.getLastName());
						interEntity.setIntrType("common");
						interEntity.setCustBalance(t.getAvailableBalance());
						interEntity.setCustomerId(t.getId());
						if (t.getBalanceToWithdraw() != null) {
							interEntity.setAmountWithInterestRate((t.getBalanceToWithdraw()) + totalInterest);
						} else {
							interEntity.setAmountWithInterestRate(totalInterest);
						}
						interEntity.setInterestRate(interestRate);
						interEntity.setCustInteresBalance(totalInterest);
						interEntity.setIsAdded(false);
						// interEntity.setCreatedAt(LocalDate.now());
						interEntity.setCreatedAt(date);
						System.out.println("date = " + interEntity.getCreatedAt());
						interEntity.setApprovedAt(LocalDate.now());
						InterestRateDetailsEntity savedInterest = intrRepo.save(interEntity);
						if (savedInterest != null) {
							result = "تم توليد نسبة الأرباح";
						} else {
							result = "فشل توليد نسبة الأرباح";
						}
					} else if (t.getAvailableBalance() != null && t.getAvailableBalance() > 0
							&& (t.getSuspendedBalance() == null || t.getSuspendedBalance() == 0)) {
						// customer has only available balance
						Float interest = (t.getAvailableBalance()) * (interestRate / 100);
						// calculate interest for suspended balance
						InterestRateDetailsEntity interEntity = new InterestRateDetailsEntity();
						// fill in interest details
						interEntity.setAccountNo(t.getAccountNo());
						interEntity.setFirstName(t.getFirstName());
						interEntity.setMiddleName(t.getMiddleName());
						interEntity.setLastName(t.getLastName());
						interEntity.setIntrType("common");
						interEntity.setCustBalance(t.getAvailableBalance());
						interEntity.setCustomerId(t.getId());
						if (t.getBalanceToWithdraw() != null) {
							interEntity.setAmountWithInterestRate((t.getBalanceToWithdraw()) + interest);
						} else {
							interEntity.setAmountWithInterestRate(interest);
						}
						interEntity.setInterestRate(interestRate);
						interEntity.setCustInteresBalance(interest);
						interEntity.setIsAdded(false);
						// interEntity.setCreatedAt(LocalDate.now());
						interEntity.setCreatedAt(date);
						interEntity.setApprovedAt(LocalDate.now());
						InterestRateDetailsEntity savedInterest = intrRepo.save(interEntity);
						if (savedInterest != null) {
							result = "تم توليد نسبة الأرباح";
						} else {
							result = "فشل توليد نسبة الأرباح";
						}
					} else if (t.getSuspendedBalance() != null && t.getSuspendedBalance() > 0
							&& (t.getAvailableBalance() == null || t.getAvailableBalance() == 0)) {
						// customer has only suspended balance
						Float interestForSuspendedBalance = (((t.getSuspendedBalance()) * (interestRate / 100)) / 30)
								* (t.getWorkingDays());
						// calculate interest for suspended balance
						InterestRateDetailsEntity interEntity = new InterestRateDetailsEntity();
						// fill in interest details
						interEntity.setAccountNo(t.getAccountNo());
						interEntity.setFirstName(t.getFirstName());
						interEntity.setMiddleName(t.getMiddleName());
						interEntity.setLastName(t.getLastName());
						interEntity.setIntrType("common");
						interEntity.setCustBalance(t.getAvailableBalance());
						interEntity.setCustomerId(t.getId());
						if (t.getBalanceToWithdraw() != null) {
							interEntity.setAmountWithInterestRate(
									(t.getBalanceToWithdraw()) + interestForSuspendedBalance);
						} else {
							interEntity.setAmountWithInterestRate(interestForSuspendedBalance);
						}
						interEntity.setInterestRate(interestRate);
						interEntity.setCustInteresBalance(interestForSuspendedBalance);
						interEntity.setIsAdded(false);
						// interEntity.setCreatedAt(LocalDate.now());
						interEntity.setCreatedAt(date);
						System.out.println("date = " + interEntity.getCreatedAt());
						interEntity.setApprovedAt(LocalDate.now());
						InterestRateDetailsEntity savedInterest = intrRepo.save(interEntity);
						if (savedInterest != null) {
							result = "تم توليد نسبة الأرباح";
						} else {
							result = "فشل توليد نسبة الأرباح";
						}
					}
				}
			} else {
				result = "لا يوجد مستثمرين لديهم رصيد";
			}
		} else {
			result = "الحقول فارغة";
		}
		return result;
	}

	@Override
	public String addSpecificInterest(Float intersetRatePlus, Long accountNo) {
		String result = "الحقول فارغة";
		if (intersetRatePlus != null && intersetRatePlus > 0 && accountNo != null) {
			CustomerEntity customerEntity = custRepo.findByAccountNo(accountNo).get();
			if (customerEntity.getAvailableBalance() != null) {
				Float interest = (customerEntity.getTotalBalance()) * (intersetRatePlus / 100);
				// add interest details
				InterestRateDetailsEntity interEntity = new InterestRateDetailsEntity();
				interEntity.setAccountNo(customerEntity.getAccountNo());
				interEntity.setFirstName(customerEntity.getFirstName());
				interEntity.setMiddleName(customerEntity.getMiddleName());
				interEntity.setLastName(customerEntity.getLastName());
				interEntity.setIntrType("special");
				interEntity.setCustBalance(customerEntity.getAvailableBalance());
				interEntity.setCustomerId(customerEntity.getId());
				if (customerEntity.getBalanceToWithdraw() != null) {
					interEntity.setAmountWithInterestRate((customerEntity.getBalanceToWithdraw()) + interest);
				} else {
					interEntity.setAmountWithInterestRate(interest);
				}
				interEntity.setInterestRatePlus(intersetRatePlus);
				interEntity.setCustInteresBalance(interest);
				interEntity.setIsAdded(false);
				interEntity.setCreatedAt(LocalDate.now());
				interEntity.setApprovedAt(LocalDate.now());
				InterestRateDetailsEntity savedInterest = intrRepo.save(interEntity);
				if (savedInterest != null) {
					result = "تم توليد نسبة الأرباح الخاصة";
				} else {
					result = " فشل توليد نسبة الأرباح الخاصة";
				}
			} else {
				Float interest = (customerEntity.getTotalBalance()) * (intersetRatePlus / 100);
				// add interest details
				InterestRateDetailsEntity interEntity = new InterestRateDetailsEntity();
				interEntity.setAccountNo(customerEntity.getAccountNo());
				interEntity.setFirstName(customerEntity.getFirstName());
				interEntity.setMiddleName(customerEntity.getMiddleName());
				interEntity.setLastName(customerEntity.getLastName());
				interEntity.setIntrType("special");
				interEntity.setCustBalance(0F);
				interEntity.setCustomerId(customerEntity.getId());
				if (customerEntity.getBalanceToWithdraw() != null) {
					interEntity.setAmountWithInterestRate((customerEntity.getBalanceToWithdraw()) + interest);
				} else {
					interEntity.setAmountWithInterestRate(0F);
				}
				interEntity.setInterestRatePlus(0F);
				interEntity.setCustInteresBalance(0F);
				interEntity.setIsAdded(false);
				interEntity.setCreatedAt(LocalDate.now());
				interEntity.setApprovedAt(LocalDate.now());
				InterestRateDetailsEntity savedInterest = intrRepo.save(interEntity);
				if (savedInterest != null) {
					result = "تم توليد نسبة الأرباح الخاصة";
				} else {
					result = " فشل توليد نسبة الأرباح الخاصة";
				}
			}
		} else {
			result = "الحقول فارغة";
		}
		return result;
	}

	@Override
	public String approveTransfer(Integer id) {
		String result = "الرجاء تسجيل الدخول لتتمكن من إجراء العملية";
		Optional<TransferEntity> optional = trRepo.findById(id);
		if (optional != null && optional.isPresent()) {
			TransferEntity transferEntity = optional.get();
			transferEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
			transferEntity.setApprovedAt(LocalDate.now());
			transferEntity.setReceivedAt(LocalDate.now());
			TransferEntity save = trRepo.save(transferEntity);
			if (save != null) {
				// update receiver balance
				CustomerEntity customerEntity = custRepo.findByAccountNo(save.getAccountNo()).get();
				if (customerEntity.getBalanceToWithdraw() != null) {
					customerEntity.setBalanceToWithdraw(customerEntity.getBalanceToWithdraw() + save.getAmount());
					if (customerEntity.getTotalBalance() != null) {
						customerEntity.setTotalBalance(customerEntity.getTotalBalance() + save.getAmount());
					} else {
						customerEntity.setTotalBalance(save.getAmount());
					}
				} else {
					customerEntity.setBalanceToWithdraw(save.getAmount());
					if (customerEntity.getTotalBalance() != null) {
						customerEntity.setTotalBalance(customerEntity.getTotalBalance() + save.getAmount());
					} else {
						customerEntity.setTotalBalance(save.getAmount());
					}
				}
				CustomerEntity save2 = custRepo.save(customerEntity);
				TransactionEntity findByProcessId = txRepo.findByProcessId(save.getId());
				findByProcessId.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
				findByProcessId.setApprovedAt(LocalDate.now());
				findByProcessId.setReceivedAt(LocalDate.now());
				if (save2.getTotalBalance() != null && save2.getTotalBalance() > 0) {
					findByProcessId.setCustomerBalance(save2.getTotalBalance());
				} else {
					findByProcessId.setCustomerBalance(0F);
				}
				txRepo.save(findByProcessId);
				result = "تمت الموافقة على طلب التحويل";
			}
		} else {
			result = "رقم العملية غير موجود";
		}

		return result;
	}

	@Override
	public String approveDeposit(Integer id, String date) {
		DepositeRequestEntity depositeRequestEntity = dpRep.findById(id).get();
		depositeRequestEntity.setStatus(CustomerConstant.TR_STATUS_APPROVED);
		depositeRequestEntity.setApprovedAt(LocalDate.now());
		DepositeRequestEntity save = dpRep.save(depositeRequestEntity);
		Optional<CustomerEntity> customerEntity = custRepo.findByAccountNo(depositeRequestEntity.getAccountNo());
		if (save != null) {
			// update the transaction status
			TransactionEntity findByProcessId = txRepo.findByProcessId(save.getId());
			findByProcessId.setStatus(CustomerConstant.TR_STATUS_APPROVED);
			findByProcessId.setApprovedAt(LocalDate.now());
			if (customerEntity.get().getTotalBalance() != null && customerEntity.get().getTotalBalance() > 0) {
				findByProcessId.setCustomerBalance(customerEntity.get().getTotalBalance());
			} else {
				findByProcessId.setCustomerBalance(0F);
			}
			TransactionEntity save2 = txRepo.save(findByProcessId);
			// get customer by his account no
			// Optional<CustomerEntity> customerEntity =
			// custRepo.findByAccountNo(depositeRequestEntity.getAccountNo());
			// send email to custmer
			try {
				email.sendDepositApproved(customerEntity.get(), date, save2.getTransactionNo().toString());
			} catch (MessagingException | IOException | TemplateException e) {
				e.printStackTrace();
			}
			return "تم تأكيد عملية الإيداع";
		}
		return null;
	}

	@Override
	public Long getInActiveCustomerCount() {
		return custRepo.getInActiveCustomerCount();
	}

	@Override
	public Long getPenddingWithdrawCounts() {
		return wrRepo.getPenddingWithdrawsCount();
	}

	@Override
	public Long getPenddingTransferCounts() {
		return trRepo.getPenddingTransferCounts();
	}

	@Override
	public Long getPenddingDepositCounts() {
		return dpRep.getPenddingDepositsCount();
	}

	@Override
	public Float getPenddingBalanceToWithdraw() {
		List<WithdrawRequestEntity> list = wrRepo.findAll();
		Float count = 0F;
		for (WithdrawRequestEntity t : list) {
			count += t.getAmount();
		}
		return count;
	}

	@Override
	public Long getLastTransferCounts() {
		return trRepo.getLastCompleteTransferCount();
	}

	@Override
	public Long getLastWithdrawCounts() {
		return wrRepo.getLastCompleteWithdrawsCount();
	}

	@Override
	public String approveInvest(Integer id) {
		String result = "فشلت عملية عملية تشغيل الأرباح";
		InvestInterestEntity investInterestEntity = investRepo.findById(id).get();
		investInterestEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
		investInterestEntity.setApprovedAt(LocalDate.now());
		investInterestEntity.setReceivedAt(LocalDate.now());
		InvestInterestEntity saveedInvest = investRepo.save(investInterestEntity);
		if (saveedInvest != null) {
			// update customer balance
			CustomerEntity customerEntity = custRepo.findByAccountNo(saveedInvest.getAccountNo()).get();
			customerEntity.setTotalBalance(customerEntity.getTotalBalance() + saveedInvest.getAmount());
			customerEntity.setAvailableBalance(customerEntity.getAvailableBalance() + saveedInvest.getAmount());
			customerEntity.setSuspendedBalance(0F);
			// update transaction
			TransactionEntity txEntity = txRepo.findByProcessId(saveedInvest.getId());
			txEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
			txEntity.setApprovedAt(LocalDate.now());
			txEntity.setReceivedAt(LocalDate.now());
			txRepo.save(txEntity);
			// send email to customer
			try {
				email.sendInvestMoneyRequestComplete(customerEntity);
			} catch (MessagingException | IOException | TemplateException e) {
				e.printStackTrace();
			}
			result = "تم تشغيل الأرباح بنجاح";
		} else {
			result = "فشلت عملية عملية تشغيل الأرباح";
		}
		return result;
	}

	@Override
	public List<InvestInterestEntity> viewPendingInvestRequests() {
		Optional<List<InvestInterestEntity>> viewPendingInvestInterestRequest = investRepo
				.viewPendingInvestInterestRequest();
		if (!viewPendingInvestInterestRequest.get().isEmpty() && viewPendingInvestInterestRequest != null) {
			return viewPendingInvestInterestRequest.get();
		}
		return null;
	}

	@Override
	public List<InterestRateDetailsEntity> viewTemporarAddedInterest() {
		return intrRepo.findAllPenddingInterest();
	}

	@Override
	public String confirmInterest(Integer id) {
		String result = "فشل إضافة نسبة الأرباح";
		InterestRateDetailsEntity interest = intrRepo.findById(id).get();
		if (interest != null) {
			// get customer by his account number
			CustomerEntity customerEntity = custRepo.findByAccountNo(interest.getAccountNo()).get();

			if (customerEntity.getBalanceToWithdraw() != null && customerEntity != null) {
				customerEntity.setBalanceToWithdraw(
						(customerEntity.getBalanceToWithdraw()) + interest.getCustInteresBalance());
				customerEntity.setTotalBalance((customerEntity.getTotalBalance()) + interest.getCustInteresBalance());
				if (customerEntity.getSuspendedBalance() != null && customerEntity.getSuspendedBalance() > 0) {
					if (customerEntity.getAvailableBalance() != null) {
						customerEntity.setAvailableBalance(
								customerEntity.getAvailableBalance() + customerEntity.getSuspendedBalance());
						customerEntity.setSuspendedBalance(0F);
					} else {
						customerEntity.setAvailableBalance(customerEntity.getSuspendedBalance());
						customerEntity.setSuspendedBalance(0F);
					}
				}
			} else {
				customerEntity.setBalanceToWithdraw(interest.getCustInteresBalance());
				customerEntity.setTotalBalance(customerEntity.getTotalBalance() + interest.getCustInteresBalance());
				if (customerEntity.getAvailableBalance() != null) {
					if (customerEntity.getSuspendedBalance() != null) {
						customerEntity.setAvailableBalance(
								customerEntity.getAvailableBalance() + customerEntity.getSuspendedBalance());
						customerEntity.setSuspendedBalance(0F);
					}
				} else {
					customerEntity.setAvailableBalance(customerEntity.getSuspendedBalance());
					customerEntity.setSuspendedBalance(0F);
				}
			}
			if (interest.getIntrType().equals("common")) {
				customerEntity.setInterestRate(interest.getInterestRate());
			} else {
				customerEntity.setIntersetRatePlus(interest.getInterestRatePlus());
			}
			CustomerEntity saveedEntity = custRepo.save(customerEntity);
			if (saveedEntity != null) {
				// update interest details
				interest.setIsAdded(true);
				InterestRateDetailsEntity save = intrRepo.save(interest);
				// add interest report details
				InterestReportEntity intrEntity = new InterestReportEntity();
				intrEntity.setAccountNo(save.getAccountNo());
				intrEntity.setFullName(save.getFirstName() + " " + save.getLastName());
				intrEntity.setInterestRate(save.getInterestRate());
				intrEntity.setCustomerAvailableBalance(saveedEntity.getAvailableBalance());
				intrEntity.setCustomerBalance(save.getCustBalance());
				intrEntity.setMonth(save.getCreatedAt());
				intrEntity.setCustomerInterestBalance(save.getCustInteresBalance());
				repoRepo.save(intrEntity);
				// send email to customer
				if (interest.getIntrType().equals("common")) {
					try {
						email.sendCommonInterest(customerEntity, interest.getApprovedAt().toString());
					} catch (MessagingException | IOException | TemplateException e) {
						e.printStackTrace();
					}
				} else {
					try {
						email.sendSpecialInterest(customerEntity, interest.getApprovedAt().toString());
					} catch (MessagingException | IOException | TemplateException e) {
						e.printStackTrace();
					}
				}
				result = "تمت إضافة نسبة الأرباح إلى المستثمر";
			} else {
				result = "فشل إضافة نسبة الأرباح";
			}
		} else {
			result = "فشل إضافة نسبة الأرباح";
		}
		return result;
	}

	private Long generateTransactionNo() {
		Random rand = new Random();
		Long num = (long) (rand.nextInt(9000) + 1000);
		Optional<TransactionEntity> findbByTrNo = txRepo.findByTransactionNo(num);
		if (findbByTrNo.isPresent() && findbByTrNo.get() != null) {
			return num + 1;
		}
		return num;
	}

	@Override
	public String updateCustomer(CustomerEntity cust) {
		CustomerEntity save = custRepo.save(cust);
		if (save != null) {
			return "تم التعديل بنجاح";
		}
		return "فشل تعديل بيانات المستثمر";
	}

	@Override
	public long getCurrentMonthInterestCount(LocalDate date) {
		List<InterestReportEntity> interest = repoRepo.getByMonth(date.getMonthValue());
		long count = 0;
		for (InterestReportEntity i : interest) {
			count += i.getCustomerInterestBalance();
		}
		return count;
	}

	@Override
	public long getCurrentYearInterestCount(LocalDate date) {
		List<InterestReportEntity> byyear = repoRepo.getByyear(date.getYear());
		long count = 0;
		for (InterestReportEntity i : byyear) {
			count += i.getCustomerInterestBalance();
		}
		return count;
	}

	@Override
	public long getAllInterestCount() {
		List<InterestReportEntity> findAll = repoRepo.findAll();
		long count = 0;
		for (InterestReportEntity i : findAll) {
			count += i.getCustomerInterestBalance();
		}
		return count;
	}

	@Override
	public List<InterestReportEntity> getMonthReport(LocalDate date) {
		return repoRepo.getByMonth(date.getMonthValue());
	}

	@Override
	public List<InterestReportEntity> getYearReport(LocalDate date) {
		return repoRepo.getByyear(date.getYear());
	}

	@Override
	public List<InterestReportEntity> getCustomerMonthlyInterest(long account, LocalDate type) {
		return repoRepo.getByMonth(type.getMonthValue(), account);
	}

	@Override
	public List<InterestReportEntity> getCustomerYearlyInterest(long account, LocalDate type) {
		return repoRepo.getByyear(type.getYear(), account);
	}

	@Override
	public long getCustomerMonthlyInterestCount(long account) {
		List<InterestReportEntity> byMonth = repoRepo.getByMonth(LocalDate.now().getMonthValue(), account);
		long count = 0;
		for (InterestReportEntity i : byMonth) {
			count += i.getCustomerInterestBalance();
		}
		return count;
	}

	@Override
	public long getCustomerYearlyInterestCount(long account) {
		List<InterestReportEntity> byMonth = repoRepo.getByyear(LocalDate.now().getYear(), account);
		long count = 0;
		for (InterestReportEntity i : byMonth) {
			count += i.getCustomerInterestBalance();
		}
		return count;
	}

	@Override
	public long getCustomertotalInterestCount(long account) {
		List<InterestReportEntity> byMonth = repoRepo.findByAccountNo(account);
		long count = 0;
		for (InterestReportEntity i : byMonth) {
			count += i.getCustomerInterestBalance();
		}
		return count;
	}

	@Override
	public List<InterestReportEntity> getCustomerAllInterest(long account, LocalDate type) {
		return repoRepo.findByAccountNo(account);
	}
}