package org.dello.reg.admin.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class InterestReportEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUST_SEQ")
	@SequenceGenerator(name = "CUST_SEQ", sequenceName = "CUSTOMER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private long id;
	@Column(name = "account_no", length = 10)
	private long accountNo;
	@Column(name = "full_name", length = 150)
	private String fullName;
	@Column(name = "interest_rate", length = 15)
	private Float interestRate;
	@Column(name = "customer_balance", length = 15)
	private Float customerBalance;
	@Column(name = "customer_available_balance", length = 15)
	private Float customerAvailableBalance;
	@Column(name = "customer_interest_balance", length = 15)
	private Float customerInterestBalance;
	@Column(name = "month")
	private LocalDate month;
}