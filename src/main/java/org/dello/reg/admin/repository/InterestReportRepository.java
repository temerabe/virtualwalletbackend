package org.dello.reg.admin.repository;

import java.util.List;

import org.dello.reg.admin.entity.InterestReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InterestReportRepository extends JpaRepository<InterestReportEntity, Long> {
	// @Query("select e from InterestReportEntity e where month(e.month) = ?1")
	@Query("select e from InterestReportEntity e where month(e.month) =:month")
	List<InterestReportEntity> getByMonth(int month);

	// @Query("select e from InterestReportEntity e where month(e.month) = ?1 and
	// accountNo = ?2")
	@Query("select e from InterestReportEntity e where month(e.month) =:month and accountNo =:account")
	List<InterestReportEntity> getByMonth(int month, long account);

	// @Query("select e from InterestReportEntity e where year(e.month) = ?1")
	@Query("select e from InterestReportEntity e where year(e.month) =:year")
	List<InterestReportEntity> getByyear(int year);

	// @Query("select e from InterestReportEntity e where year(e.month) = ?1 and
	// accountNo = ?2")
	@Query("select e from InterestReportEntity e where year(e.month) =:year and accountNo =:account")
	List<InterestReportEntity> getByyear(int year, long account);

	List<InterestReportEntity> findByAccountNo(long account);
}