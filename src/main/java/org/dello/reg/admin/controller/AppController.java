package org.dello.reg.admin.controller;

import java.beans.PropertyEditor;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.dello.exceptions.UserNotFoundException;
import org.dello.login.service.LoginService;
import org.dello.reg.admin.entity.InterestReportEntity;
import org.dello.reg.admin.service.AppService;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.InterestRateDetailsEntity;
import org.dello.reg.customer.entity.InvestInterestEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.dello.reg.customer.model.CustomerBalance;
import org.dello.reg.customer.repository.InvestInterestRepository;
import org.dello.reg.user.entity.UserEntity;
import org.dello.reg.user.model.User;
import org.dello.reg.user.repository.UserRepository;
import org.dello.reg.user.service.UserService;
import org.dello.util.PasswordEncryptionUsingAES;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.PropertiesEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {
	@Autowired
	private AppService app;
	@Autowired
	private LoginService loginService;
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private InvestInterestRepository invesRepo;

	@GetMapping(value = "admin/customers", produces = "application/json")
	public List<CustomerEntity> retreiveCustomers() {
		List<CustomerEntity> customers = app.getCustomers();
		if (customers == null || customers.isEmpty()) {
			throw new UserNotFoundException("No Customers Available");
		}
		return customers;
	}

	@GetMapping("/admin/interest/customer/{account}/and/{type}")
	public List<InterestReportEntity> getCustomerInterest(@PathVariable(name = "account") long account,
			@PathVariable(name = "type") String type) {
		if (type.equals("year")) {
			return app.getCustomerYearlyInterest(account, LocalDate.now());
		}
		if (type.equals("month")) {
			return app.getCustomerMonthlyInterest(account, LocalDate.now());
		}
		if (type.equals("all")) {
			return app.getCustomerAllInterest(account, LocalDate.now());
		}
		return null;
	}

	@GetMapping("/admin/interest/customer/m/count/{account}")
	public long getCustomerMonthCcount(@PathVariable(name = "account") long account) {
		return app.getCustomerMonthlyInterestCount(account);
	}

	@GetMapping("/admin/interest/customer/y/count/{account}")
	public long getCustomerYearCcount(@PathVariable(name = "account") long account) {
		return app.getCustomerYearlyInterestCount(account);
	}

	@GetMapping("/admin/interest/customer/all/count/{account}")
	public long getCustomerAllCcount(@PathVariable(name = "account") long account) {
		return app.getCustomertotalInterestCount(account);
	}

	@GetMapping("/admin/interest/count/m/{month}")
	public long getCurrentMonthInterestCount(@PathVariable(name = "month") LocalDate date) {
		return app.getCurrentMonthInterestCount(date);
	}

	@GetMapping("/admin/interest/month/{month}")
	public List<InterestReportEntity> getMonthReport(@PathVariable(name = "month") LocalDate date) {
		return app.getMonthReport(date);
	}

	@GetMapping("/admin/interest/year/{year}")
	public List<InterestReportEntity> getYearReport(@PathVariable(name = "year") LocalDate date) {
		return app.getYearReport(date);
	}

	@GetMapping("/admin/interest/count/y/{year}")
	public long getCurrentInterestCount(@PathVariable(name = "year") LocalDate date) {
		return app.getCurrentYearInterestCount(date);
	}

	@GetMapping("/admin/interest/all/count")
	public long getAllInterestCount() {
		return app.getAllInterestCount();
	}

	@GetMapping(value = "admin/reg/status", produces = "application/json")
	public List<CustomerEntity> viewPendingCustReg() {
		List<CustomerEntity> viewCusPendingReg = app.viewCusPendingReg();
		if (viewCusPendingReg == null || viewCusPendingReg.isEmpty()) {
			throw new UserNotFoundException("No Pendding Customers for Registration");
		}
		return viewCusPendingReg;
	}

	@GetMapping(value = "admin/wr/status", produces = "application/json")
	public List<WithdrawRequestEntity> viewWithdrawRequest() {
		List<WithdrawRequestEntity> viewPendingWithdrawRequest = app.viewPendingWithdrawRequest();
		System.out.println("The list size is " + viewPendingWithdrawRequest.size());
		if (viewPendingWithdrawRequest == null || viewPendingWithdrawRequest.isEmpty()) {
			throw new UserNotFoundException("No Pendding Withdraw Requests");
		}
		return viewPendingWithdrawRequest;
	}

	@GetMapping(value = "admin/tr/status", produces = "application/json")
	public List<TransferEntity> viewTransferRequest() {
		List<TransferEntity> viewPendingTransferRequest = app.viewPendingTransferRequest();
		if (viewPendingTransferRequest == null || viewPendingTransferRequest.isEmpty()) {
			throw new UserNotFoundException("No Transfer Requests");
		}
		return viewPendingTransferRequest;
	}

	@GetMapping(value = "admin/dp/status", produces = "application/json")
	public List<DepositeRequestEntity> viewDepositRequest() {
		List<DepositeRequestEntity> viewPendingDepositRequest = app.viewPendingDepositRequest();
		if (viewPendingDepositRequest == null || viewPendingDepositRequest.isEmpty()) {
			throw new UserNotFoundException("No Pendding Deposit request");
		}
		return viewPendingDepositRequest;
	}

	@GetMapping(value = "admin/invest/status", produces = "application/json")
	public List<InvestInterestEntity> viewInvestMoneyRequest() {
		List<InvestInterestEntity> list = invesRepo.viewPendingInvestInterestRequest().get();
		if (list == null || list.isEmpty()) {
			throw new UserNotFoundException("No Pendding Deposit request");
		}
		return list;
	}

	@GetMapping(value = "admin/customers/active", produces = "application/json")
	public List<CustomerEntity> getActiveCustomers() {
		List<CustomerEntity> activeCustomers = app.getActiveCustomers();
		if (activeCustomers == null || activeCustomers.isEmpty()) {
			throw new UserNotFoundException("No Active Customers");
		}
		return activeCustomers;
	}

	@GetMapping("/customers/inactive/count")
	public Long getInActiveCustomersCount() {
		Long inActiveCustomerCount = app.getInActiveCustomerCount();
		return inActiveCustomerCount;
	}

	@GetMapping("/customers/penddingwithdraws/count")
	public Long getPenddingWithdrawCounts() {
		Long penddingWithdrawCounts = app.getPenddingWithdrawCounts();
		if (penddingWithdrawCounts != null) {
			return penddingWithdrawCounts;
		}
		return null;
	}

	@GetMapping("/customers/penddingtransfer/count")
	Long getPenddingTransferCounts() {
		Long penddingTransferCounts = app.getPenddingTransferCounts();
		if (penddingTransferCounts != null) {
			return penddingTransferCounts;
		}
		return null;
	}

	@GetMapping("/customers/penddingdeposit/count")
	Long getPenddingDepositCounts() {
		Long penddingDepositCounts = app.getPenddingDepositCounts();
		if (penddingDepositCounts != null) {
			return penddingDepositCounts;
		}
		return null;
	}

	@GetMapping("/customers/penddingwithdrawbalance/count")
	Float getPenddingBalanceToWithdraw() {
		Float penddingBalanceToWithdraw = app.getPenddingBalanceToWithdraw();
		if (penddingBalanceToWithdraw != null) {
			return penddingBalanceToWithdraw;
		}
		return null;
	}

	@GetMapping("/customers/lasttransfer/count")
	Long getLastTransferCounts() {
		Long lastTransferCounts = app.getLastTransferCounts();
		if (lastTransferCounts != null) {
			return lastTransferCounts;
		}
		return null;
	}

	@GetMapping("/customers/lastwithdraw/count")
	Long getLastWithdrawCounts() {
		Long lastWithdrawCounts = app.getLastWithdrawCounts();
		if (lastWithdrawCounts != null) {
			return lastWithdrawCounts;
		}
		return null;
	}

	@GetMapping("/customer/tmp/interest")
	public List<InterestRateDetailsEntity> getTemporaryAddedInterest() {
		return app.viewTemporarAddedInterest();
	}

	@PutMapping("admin/customers/update")
	public String updaeCustomer(@RequestBody CustomerEntity customer) {
		return app.updateCustomer(customer);
	}

	@PutMapping(value = "admin/customers/addbalance")
	public String addBalance(@RequestBody CustomerBalance custBalance) {
		System.out.println(custBalance);
		return app.addBalance(custBalance);
	}

	@PutMapping("admin/reg/approve/{custId}")
	public String approveRegistration(@PathVariable("custId") Integer custId) {

		String activateCustomer = app.activateCustomer(custId);
		return activateCustomer;

	}

	@PutMapping("admin/wr/approve/{id}")
	String approvePendingWithdraw(@PathVariable("id") Integer id, @RequestParam(name = "date") String date) {
		System.out.println("id is = " + id + "date is = " + date);
		String approveWithdraw = app.approveWithdraw(id, date);
		return approveWithdraw;
	}

	@PutMapping("admin/tr/approve/{id}")
	String approvePendingTransfer(@PathVariable("id") Integer id) {
		return app.approveTransfer(id);
	}

	@PutMapping("admin/dp/approve/{id}")
	String approvePendingDeposit(@PathVariable("id") Integer id, @RequestParam(name = "date") String date) {
		return app.approveDeposit(id, date);
	}

	@PutMapping("admin/invest/approve/{id}")
	String approvePendingDeposit(@PathVariable("id") Integer id) {
		return app.approveInvest(id);
	}

	@PutMapping("admin/customers/activate/{custId}")
	public String activateCustomer(@PathVariable(name = "custId") Integer custId) {
		return loginService.activateCustomer(custId);
	}

	@PutMapping("admin/customers/deactivate/{custId}")
	public String deActivateCustomer(@PathVariable(name = "custId") Integer custId) {
		return loginService.deActivateCustomer(custId);
	}

	@PutMapping("admin/users/activate/{userId}")
	public String activateUser(@PathVariable(name = "userId") Integer userId) {
		return loginService.activateUser(userId);
	}

	@PutMapping("admin/users/deactivate/{userId}")
	public String deActivateUser(@PathVariable(name = "userId") Integer userId) {
		return loginService.deActivateUser(userId);
	}

	@PutMapping("admin/customers/interest")
	public String addInterest(@RequestParam(name = "interestRate") Float interestRate,
			@RequestParam(name = "date") LocalDate date) {
		return app.addCommonInterest(interestRate, date);
	}

	@PutMapping("admin/customers/interestPlus")
	public String addInterestPlus(@RequestParam(name = "interestRate") Float interestRate,
			@RequestParam(name = "accountNo") Long accountNo) {
		return app.addSpecificInterest(interestRate, accountNo);
	}

	@PutMapping("admin/confirm/interest")
	public String confirmInterest(@RequestParam(name = "id") Integer id) {
		return app.confirmInterest(id);
	}

	@PostMapping("amdin/ruser")
	public UserEntity initialUser(@RequestBody User user) {
		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(user, entity);
		String encryptedPass = "";
		try {
			encryptedPass = PasswordEncryptionUsingAES.encryptPassword("test");
		} catch (Exception e) {
			e.printStackTrace();
		}
		entity.setCreatedDate(LocalDate.now());
		entity.setPassword(encryptedPass);
		UserEntity save = userRepo.save(entity);
		return save;
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		PropertyEditor editor = new PropertiesEditor() {
			@Override
			public void setAsText(String text) throws IllegalArgumentException {
				if (!text.trim().isEmpty()) {
					super.setValue(LocalDate.parse(text.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd")));
				}
			}

			@Override
			public String getAsText() {
				if (super.getValue() == null) {
					return null;
				}
				LocalDate value = (LocalDate) super.getValue();
				return value.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
			}
		};
		binder.registerCustomEditor(LocalDate.class, editor);
	}
}