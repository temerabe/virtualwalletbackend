package org.dello.reg.admin.constant;

public class AppConstant {
	public static final String ACCOUNT_ACTIVATED = "تم تفعيل الحساب";
	public static final String ACCOUNT_NOT_ACTIVATED = "فشل تفعيل الحساب";
	public static final String ACCOUNT_DEACTIVATED = "تم إلغاء تفعيل الحساب";
	public static final String ACCOUNT_NOt_DEACTIVATED = "لم يتم إلغاء تفعيل الحساب ";
	public static final String ROLE_ADMIN = "admin";
	public static final String ROLE_CUSTOMER = "customer";
	public static final String ROLE_EMPLOYEE = "user";
	public static final String ACCOUNT_STATUS_ACTIVE = "active";
	public static final String ACCOUNT_STATUS_INACTIVE = "inactive";
	public static final String BALANCE_ADDED = "تم إرسال طلب تشغيل الأرباح لمدير المنظومة";
	public static final String BALANCE_NOT_ADDED = "فشل إضافة الرصيد ، ليس لديك رصيد كافي للإضافة";
	public static final String INTEREST_ADDED = "تمت إضافة الأرباح بنجاح";
	public static final String INTEREST_NOT_ADDED = "فشل إضافة الأرباح ";
}
