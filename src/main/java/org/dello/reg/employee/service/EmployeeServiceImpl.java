package org.dello.reg.employee.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import javax.mail.MessagingException;

import org.dello.reg.admin.constant.AppConstant;
import org.dello.reg.customer.constant.CustomerConstant;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.TransactionEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.dello.reg.customer.repository.CustomerRepository;
import org.dello.reg.customer.repository.DepositRepository;
import org.dello.reg.customer.repository.TransactionRepository;
import org.dello.reg.customer.repository.TransferRepository;
import org.dello.reg.customer.repository.WithdrawRepository;
import org.dello.util.EmailSenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import freemarker.template.TemplateException;

@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	private TransactionRepository txRepo;
	@Autowired
	private CustomerRepository custRepo;
	@Autowired
	private WithdrawRepository wrRepo;
	@Autowired
	private TransferRepository trRepo;
	@Autowired
	private DepositRepository dpRepo;
	@Autowired
	private EmailSenderService sendEmail;

	@Override
	public TransactionEntity getCutomerProcess(Long transactionId) {
		return txRepo.completeTransaction(transactionId);
	}

	@Override
	public CustomerEntity getCustomer(Long accountNo) {
		return custRepo.findByAccountNo(accountNo).get();
	}

	@Override
	public String executeTransaction(Long trNo) {
		String result = "فشلت العملية ، عاود مرة أخرى";
		if (trNo != null) {
			// get the transaction based on transaction's no.
			TransactionEntity completeTransaction = txRepo.completeTransaction(trNo);
			// update the transactions
			completeTransaction.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
			completeTransaction.setReceivedAt(LocalDate.now());
			TransactionEntity save = txRepo.save(completeTransaction);
			// update the customer transactions details based on transactions details.
			if (save.getTrType().equals(CustomerConstant.TX_TYPE_DEPOSIT)) {
				DepositeRequestEntity depositeRequestEntity = dpRepo.findById(save.getProcessId()).get();
				depositeRequestEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
				depositeRequestEntity.setReceivedAt(LocalDate.now());
				dpRepo.save(depositeRequestEntity);
				// update customer balance
				CustomerEntity customerEntity = custRepo.findByAccountNo(save.getAccountNo()).get();
				if (customerEntity.getAvailableBalance() == null) {
					customerEntity.setAvailableBalance(depositeRequestEntity.getAmount());
					customerEntity.setTotalBalance(depositeRequestEntity.getAmount());
				} else {
					customerEntity.setAvailableBalance(
							customerEntity.getAvailableBalance() + depositeRequestEntity.getAmount());
					customerEntity
							.setTotalBalance(customerEntity.getAvailableBalance() + depositeRequestEntity.getAmount());
				}
				custRepo.save(customerEntity);
				// send email to customer
				try {
					sendEmail.sendDepositComplete(customerEntity, save.getAmount().toString());
				} catch (MessagingException | IOException | TemplateException e) {
					e.printStackTrace();
				}

			} else if (save.getTrType().equals(CustomerConstant.TX_TYPE_WITHDRAW)) {
				WithdrawRequestEntity withdrawRequestEntity = wrRepo.findById(save.getProcessId()).get();
				withdrawRequestEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
				withdrawRequestEntity.setReceivedAt(LocalDate.now());
				WithdrawRequestEntity save2 = wrRepo.save(withdrawRequestEntity);
				// update the customer suspended balance
				CustomerEntity customerEntity = custRepo.findByAccountNo(save.getAccountNo()).get();
				customerEntity.setSuspendedBalance(customerEntity.getSuspendedBalance() - save2.getAmount());
				customerEntity.setTotalBalance(customerEntity.getTotalBalance() - save2.getAmount());
				custRepo.save(customerEntity);
			} else {
				TransferEntity transferEntity = trRepo.findById(save.getProcessId()).get();
				transferEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
				transferEntity.setReceivedAt(LocalDate.now());
				trRepo.save(transferEntity);
				// update the receiver balance
				CustomerEntity customerEntity = custRepo.findByAccountNo(transferEntity.getReceAccountNo()).get();
				customerEntity.setAvailableBalance(customerEntity.getAvailableBalance() + transferEntity.getAmount());
				custRepo.save(customerEntity);
				// update costomer balance
				CustomerEntity customerEntity2 = custRepo.findByAccountNo(transferEntity.getAccountNo()).get();
				customerEntity2
						.setBalanceToWithdraw(customerEntity2.getBalanceToWithdraw() - transferEntity.getAmount());
				custRepo.save(customerEntity2);
			}
			if (save != null) {
				result = "تم تنفيذ العملية بنجاح";
			} else {
				result = "فشلت العملية ، عاود مرة أخرى";
			}
		}
		return result;
	}

}
