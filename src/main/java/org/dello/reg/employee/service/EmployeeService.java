package org.dello.reg.employee.service;

import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.TransactionEntity;

public interface EmployeeService {
	TransactionEntity getCutomerProcess(Long transactionId);

	CustomerEntity getCustomer(Long accountNo);

	String executeTransaction(Long trNo);
}
