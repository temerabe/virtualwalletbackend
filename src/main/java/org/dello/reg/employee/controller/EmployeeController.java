package org.dello.reg.employee.controller;

import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.TransactionEntity;
import org.dello.reg.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
	@Autowired
	private EmployeeService empService;

	@GetMapping("/employee/transaction/{trNo}")
	public TransactionEntity getTransaction(@PathVariable(name = "trNo") Long transactionNo) {
		return empService.getCutomerProcess(transactionNo);
	}

	@GetMapping("/employee/customer/{accountNo}")
	public CustomerEntity getCustomer(@PathVariable(name = "accountNo") Long accountNo) {
		return empService.getCustomer(accountNo);
	}

	@PutMapping("/employee/ex/transaction")
	public String executeTransaction(@RequestParam(name = "trNo") Long trNo) {
		System.out.println("trNo is = "+trNo);
		return empService.executeTransaction(trNo);
	}
}
