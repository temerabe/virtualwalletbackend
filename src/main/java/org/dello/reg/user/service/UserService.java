package org.dello.reg.user.service;

import java.util.List;

import org.dello.reg.customer.model.Login;
import org.dello.reg.user.entity.UserEntity;
import org.dello.reg.user.model.User;

public interface UserService {
	UserEntity registerUser(User user);

	UserEntity deleteUser(User user);

	UserEntity deleteUser(Integer userId);

	UserEntity getUser(Integer userId);

	List<UserEntity> getUsers();

	UserEntity updateUser(User user);

	UserEntity authenticate(Login login);
}
