package org.dello.reg.user.service;

import java.time.LocalDate;
import java.util.List;

import org.dello.reg.admin.constant.AppConstant;
import org.dello.reg.customer.model.Login;
import org.dello.reg.user.entity.UserEntity;
import org.dello.reg.user.model.User;
import org.dello.reg.user.repository.UserRepository;
import org.dello.util.EmailSenderService;
import org.dello.util.PasswordEncryptionUsingAES;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserRepository userRepo;
	@Autowired
	private EmailSenderService emailService;

	@Override
	public UserEntity registerUser(User user) {
		UserEntity entity = new UserEntity();
		BeanUtils.copyProperties(user, entity);
		entity.setCreatedDate(LocalDate.now());
		entity.setStatus(AppConstant.ACCOUNT_STATUS_INACTIVE);
		entity.setRole(AppConstant.ROLE_EMPLOYEE);
		// encrypt password
		String encryptedPass = null;
		boolean flag = false;
		try {
			encryptedPass = PasswordEncryptionUsingAES.encryptPassword(user.getPassword());
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		if (flag) {
			entity.setPassword(encryptedPass);
		}
		UserEntity saveEntity = userRepo.save(entity);
		if (saveEntity != null) {
			// sending mail to case workers
			/*
			 * try { emailService.sendEmailToUser(user); } catch (MessagingException |
			 * IOException | TemplateException e) { e.printStackTrace(); }
			 */
		}
		return saveEntity;
	}

	@Override
	public UserEntity deleteUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserEntity deleteUser(Integer userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserEntity getUser(Integer userId) {
		return userRepo.findById(userId).get();
	}

	@Override
	public List<UserEntity> getUsers() {
		return userRepo.findAll();
	}

	@Override
	public UserEntity updateUser(User user) {

		return null;
	}

	@Override
	public UserEntity authenticate(Login login) {
		String encryptPassword = null;
		// encrypt password
		try {
			encryptPassword = PasswordEncryptionUsingAES.encryptPassword(login.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userRepo.doLogin(login.getEmail(), encryptPassword).get();
	}
}