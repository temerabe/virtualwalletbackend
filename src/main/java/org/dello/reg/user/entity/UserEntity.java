package org.dello.reg.user.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Entity
@Data
public class UserEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "US_SEQ")
	@SequenceGenerator(name = "US_SEQ", sequenceName = "USER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private Integer id;
	@Column(name = "FIRST_NAME", length = 15)
	private String firstName;
	@Column(name = "LAST_NAME", length = 15)
	private String lastName;
	@Column(name = "EMAIL", length = 100)
	private String email;
	@Column(name = "PASSWORD", length = 250)
	private String password;
	@Column(name = "PHONE_NO", length = 15)
	private Long phoneNo;
	@Column(name = "ROLE", length = 15)
	private String role;
	@Column(name = "STATUS", length = 20)
	private String status;
	@Column(name = "CREATED_DATE")
	private LocalDate createdDate;
}
