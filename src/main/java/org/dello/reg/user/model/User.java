package org.dello.reg.user.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class User {
	public User() {
		// no body
	}
	private Integer id;
	private String firstName;
	private String lastName;
	private String email;
	private String password;
	private Long phoneNo;
	private String role;
	private String status;
	private LocalDate createdDate;

	public User(Integer id, String firstName, String lastName, String email, String password, Long phoneNo, String role,
			String status, LocalDate createdDate) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.phoneNo = phoneNo;
		this.role = role;
		this.status = status;
		this.createdDate = createdDate;
	}
}
