package org.dello.reg.user.constant;

public class EmailText {

	/*
	 * public static final String APP_URL_CU =
	 * "http://localhost:4200/create-profile/"; public static final String
	 * APP_URL_RESET_PASSWORD = "http://localhost:4200/reset-password/"; public
	 * static final String CUST_IMAGE = "http://localhost:8585/customer/image/";
	 * public static final String LOGIN_URL = "http://localhost:4200";
	 */

	public static final String LOGIN_URL = "http://www.tr.ly";
	public static final String CUST_IMAGE = "https://elmotdawl.cfapps.io/customer/image/";
	public static final String APP_URL_RESET_PASSWORD = "http://www.tr.ly/reset-password/";
	public static final String APP_URL_CU = "http://www.tr.ly/create-profile/";
	public static final String APP_URL_US = "https://elmotdawl.cfapps.io/users";

}
