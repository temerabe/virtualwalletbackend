package org.dello.reg.user.controller;

import java.util.List;

import org.dello.exceptions.UserNotFoundException;
import org.dello.reg.customer.model.Login;
import org.dello.reg.user.entity.UserEntity;
import org.dello.reg.user.model.User;
import org.dello.reg.user.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
	private UserService userService;
	@PostMapping("/users")
	public Object createCustomer(@RequestBody User user) {
		UserEntity registerUser = userService.registerUser(user);
		BeanUtils.copyProperties(registerUser, user);
		return user;
	}

	@GetMapping(value = "/users", produces = "application/json")
	public List<UserEntity> getUsers() {
		return userService.getUsers();
	}

	@PutMapping("/users/{userId}/update")
	public User updateCustomer(@RequestBody User user) {
		UserEntity registerUser = userService.registerUser(user);
		BeanUtils.copyProperties(registerUser, user);
		return user;
	}

	@PostMapping(value = "/users/login", produces = "application/json")
	public Object checkUserLogin(@RequestBody Login login) {
		UserEntity authenticate = userService.authenticate(login);
		if (authenticate == null) {
			throw new UserNotFoundException("userid = " + login.getEmail());
		} else {
			return authenticate;
		}
	}
}
