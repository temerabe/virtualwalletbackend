package org.dello.reg.user.repository;

import java.util.Optional;

import org.dello.reg.user.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Integer> {
	@Query(value = "from UserEntity where email=:email and password=:password")
	Optional<UserEntity> doLogin(@Param(value = "email") String email, @Param(value = "password") String password);
}
