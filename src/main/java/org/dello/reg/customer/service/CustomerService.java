package org.dello.reg.customer.service;

import java.io.IOException;
import java.util.List;

import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.TransactionEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.dello.reg.customer.model.Customer;
import org.dello.reg.customer.model.DepositeRequest;
import org.dello.reg.customer.model.ResetPassword;
import org.dello.reg.customer.model.TransferRequest;
import org.dello.reg.customer.model.WithdrawRequest;
import org.springframework.web.multipart.MultipartFile;

public interface CustomerService {
	CustomerEntity registerCustomer(Customer cust);

	String activateCustomer(Integer custId);

	CustomerEntity deleteCustomer(CustomerEntity cust);

	CustomerEntity deleteCustomer(Integer custId);

	CustomerEntity getCustomer(Integer custId);

	List<CustomerEntity> getCustomers();

	CustomerEntity updateCustomer(CustomerEntity cust);

	public String uploadFile(MultipartFile image, Integer custId) throws IOException;

	String withdrawMoney(WithdrawRequest wrequest);

	String investMoney(Integer id, Float amount);

	String depositMoney(DepositeRequest drequest);

	String transferMoney(TransferRequest trequest);

	List<TransferEntity> getTransfersByAccountNo(Long accountNo);

	List<WithdrawRequestEntity> getWithdrawByAccountNo(Long accountNo);

	List<DepositeRequestEntity> getDepositByAccountNo(Long accountNo);

	List<TransactionEntity> transactions(Long accountNo);

	String findImageById(Integer custId);

	String findDelegImageById(Integer custId);

	String forgotPassword(String email);

	String resetPassword(ResetPassword restPassword);

	String cancelTx(Integer id);

	String sendWithdrawEmail(Integer id);

	boolean isEmailAvailable(String email);

	String findCustPassportImageById(Integer custId);

	List<TransactionEntity> getWithdrawsTx(Long accountNo);
}