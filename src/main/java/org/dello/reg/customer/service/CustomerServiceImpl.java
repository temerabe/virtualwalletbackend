package org.dello.reg.customer.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.dello.reg.admin.constant.AppConstant;
import org.dello.reg.customer.constant.CustomerConstant;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.InvestInterestEntity;
import org.dello.reg.customer.entity.TransactionEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.dello.reg.customer.model.Customer;
import org.dello.reg.customer.model.DepositeRequest;
import org.dello.reg.customer.model.ResetPassword;
import org.dello.reg.customer.model.TransferRequest;
import org.dello.reg.customer.model.WithdrawRequest;
import org.dello.reg.customer.repository.CustomerRepository;
import org.dello.reg.customer.repository.DepositRepository;
import org.dello.reg.customer.repository.InvestInterestRepository;
import org.dello.reg.customer.repository.TransactionRepository;
import org.dello.reg.customer.repository.TransferRepository;
import org.dello.reg.customer.repository.WithdrawRepository;
import org.dello.util.EmailSenderService;
import org.dello.util.MessageService;
import org.dello.util.PasswordEncryptionUsingAES;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;

import freemarker.template.TemplateException;

@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerRepository custRepo;
	@Autowired
	private ServletContext sc;
	@Autowired
	private EmailSenderService sendEmail;
	@Autowired
	private WithdrawRepository wrRepo;
	@Autowired
	private DepositRepository depRepo;
	@Autowired
	private TransferRepository trRepo;
	@Autowired
	private TransactionRepository txRepo;
	@Autowired
	private InvestInterestRepository investRepo;
	@Autowired
	private MessageService sms;
	@Autowired
	private AmazonS3Client s3client;

	@Override
	public CustomerEntity registerCustomer(Customer cust) {

		CustomerEntity entity = new CustomerEntity();
		entity.setFirstName(cust.getFirstName());
		entity.setMiddleName(cust.getMiddleName());
		entity.setLastName(cust.getLastName());
		entity.setEmail(cust.getEmail());
		entity.setAccountNo(generateAccountNo());
		entity.setIsProfileUpdated(false);
		entity.setAccountStatus(AppConstant.ACCOUNT_STATUS_INACTIVE);
		entity.setCreatedAt(LocalDate.now());
		// encrypt password
		boolean flag = false;
		String encryptedPass = null;
		try {
			encryptedPass = PasswordEncryptionUsingAES.encryptPassword(cust.getPassword());
			flag = true;
		} catch (Exception e) {
			flag = false;
			e.printStackTrace();
		}
		if (flag) {
			entity.setPassword(encryptedPass);
		}
		entity.setRole(AppConstant.ROLE_CUSTOMER);
		CustomerEntity saveEntity = custRepo.save(entity);
		// send email to customer containing the application url
		try {
			sendEmail.sendEmailToCustomer(saveEntity);
		} catch (MessagingException | IOException | TemplateException e) {
			e.printStackTrace();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println(saveEntity);
		return saveEntity;
	}

	@Override
	public CustomerEntity deleteCustomer(CustomerEntity cust) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerEntity deleteCustomer(Integer custId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerEntity getCustomer(Integer custId) {
		Optional<CustomerEntity> findById = custRepo.findById(custId);
		if (findById.isPresent()) {
			return findById.get();
		}
		return null;
	}

	@Override
	public List<CustomerEntity> getCustomers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CustomerEntity updateCustomer(CustomerEntity cust) {
		Optional<CustomerEntity> findById = custRepo.findById(cust.getId());
		if (findById.isPresent()) {
			cust = findById.get();
		}
		CustomerEntity save = custRepo.save(cust);
		save.setAccountStatus(AppConstant.ACCOUNT_STATUS_ACTIVE);
		return save;
	}

	@Override
	public String uploadFile(MultipartFile image, Integer custId) throws IOException {
		/*
		 * String UPLOADED_FORLDER = "ImagesFolder"; // get the application path String
		 * applicationPath = sc.getRealPath(""); // prepare file upload location String
		 * uploadedFiles = applicationPath + File.separator + UPLOADED_FORLDER; File
		 * uploadedFile = new File(uploadedFiles); if (!uploadedFile.exists()) {
		 * uploadedFile.mkdirs(); } // InputStream is =
		 * cust.getImage().getInputStream(); InputStream is = image.getInputStream();
		 * String desFileName = image.getOriginalFilename(); OutputStream os = new
		 * FileOutputStream(uploadedFile.getAbsolutePath() + "//" + desFileName); //
		 * copy uploaded file content to destination file IOUtils.copy(is, os); return
		 * uploadedFile + "//" + desFileName;
		 */
		/*s3client.putObject(new PutObjectRequest("mywalletapp", "photos/"+custId+"/"+image.getOriginalFilename(), new File(image.getOriginalFilename()))
				.withCannedAcl(CannedAccessControlList.PublicRead));*/
		s3client.putObject("mywalletapp","photos/"+custId+"/"+image.getOriginalFilename() , image.getInputStream(), null);
		String resourceUrl = s3client.getResourceUrl("mywalletapp", "photos/"+custId+"/"+image.getOriginalFilename());
		return resourceUrl;
	}

	@Override
	public String withdrawMoney(WithdrawRequest wrequest) {
		String result = "فشلت عملية السحب ، الرجاء المحاولة مرة أخرى";
		// load the customer's account by his accountNo
		CustomerEntity customerEntity = custRepo.findByAccountNo(wrequest.getAccountNo()).get();
		if (customerEntity != null) {
			// check whether customer has balance or not
			if (customerEntity.getTotalBalance() != null && customerEntity.getTotalBalance() > 0) {
				// check whether customer has balanceToWithdraw or not
				if (customerEntity.getBalanceToWithdraw() != null && customerEntity.getBalanceToWithdraw() > 0) {
					// check whether balanceToWithdaw greater than amount to be withdraw or not
					if (customerEntity.getBalanceToWithdraw() >= wrequest.getAmount()) {
						// complete the withdraw process
						WithdrawRequestEntity wrEntity = new WithdrawRequestEntity();
						BeanUtils.copyProperties(wrequest, wrEntity);
						// withdraw logic to be written
						wrEntity.setCreatedAt(LocalDate.now());
						wrEntity.setStatus(CustomerConstant.TR_STATUS_PENDDING);
						wrEntity.setWithdrawWay(wrequest.getWithdrawWay());
						WithdrawRequestEntity save = wrRepo.save(wrEntity);
						// update customer balance
						customerEntity
								.setBalanceToWithdraw(customerEntity.getBalanceToWithdraw() - wrequest.getAmount());
						if (customerEntity.getSuspendedBalance() == null) {
							customerEntity.setSuspendedBalance(wrequest.getAmount());
						} else {
							customerEntity
									.setSuspendedBalance(customerEntity.getSuspendedBalance() + wrequest.getAmount());
						}
						custRepo.save(customerEntity);
						// register the transaction details
						TransactionEntity txEntity = new TransactionEntity();
						txEntity.setCreatedAt(LocalDate.now());
						txEntity.setAccountNo(save.getAccountNo());
						txEntity.setTransactionNo(generateTransactionNo());
						txEntity.setTrType(CustomerConstant.TX_TYPE_WITHDRAW);
						txEntity.setAmount(wrequest.getAmount());
						txEntity.setCustPassportNo(customerEntity.getPassportNo());
						txEntity.setCustDelegName(customerEntity.getDelegFullName());
						txEntity.setCustDelegPassportNo(customerEntity.getDelegPassport());
						txEntity.setCustFirstName(customerEntity.getFirstName());
						txEntity.setCustMiddleName(customerEntity.getMiddleName());
						txEntity.setCustLastName(customerEntity.getLastName());
						txEntity.setNotes(CustomerConstant.TX_DESC_WITHDRAW);
						txEntity.setStatus(CustomerConstant.TR_STATUS_PENDDING);
						txEntity.setProcessId(save.getId());
						TransactionEntity trEntity = txRepo.save(txEntity);
						// send email to the customer
						try {
							sendEmail.sendWithdrawRequestAccepted(customerEntity);
						} catch (MessagingException | IOException | TemplateException e) {
							e.printStackTrace();
						}
						result = "تم إرسال طلب السحب لمدير المنظومة";
					} else {
						result = "ليس لديك رصيد كافي للسحب";
					}

				} else {
					result = "ليس لديك أرباح للسحب";
				}
			} else {
				result = "ليس لديك رصيد في حسابك";
			}
		}
		return result;
	}

	@Override
	public String investMoney(Integer id, Float amount) {
		String result = AppConstant.BALANCE_NOT_ADDED;
		// get the customer
		CustomerEntity customerEntity = custRepo.findById(id).get();
		if (customerEntity != null) {
			// check whether customer has balance or not
			if (customerEntity.getBalanceToWithdraw() != null && customerEntity.getBalanceToWithdraw() > 0) {
				if (customerEntity.getBalanceToWithdraw() >= amount) {
					if (customerEntity.getAvailableBalance() != null) {
						customerEntity.setAvailableBalance(customerEntity.getAvailableBalance() + amount);
						customerEntity.setBalanceToWithdraw(customerEntity.getBalanceToWithdraw() - amount);
					} else {
						customerEntity.setAvailableBalance(amount);
						customerEntity.setBalanceToWithdraw(customerEntity.getBalanceToWithdraw() - amount);
					}
					CustomerEntity save = custRepo.save(customerEntity);
					if (save != null) {
						// create investInterest request
						InvestInterestEntity invest = new InvestInterestEntity();
						invest.setAccountNo(customerEntity.getAccountNo());
						invest.setAmount(amount);
						invest.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
						invest.setName(customerEntity.getFirstName() + " " + customerEntity.getMiddleName() + " "
								+ customerEntity.getLastName());
						invest.setCreatedAt(LocalDate.now());
						invest.setApprovedAt(LocalDate.now());
						invest.setReceivedAt(LocalDate.now());
						invest.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
						InvestInterestEntity savedInvest = investRepo.save(invest);
						// register transaction details
						TransactionEntity txEntity = new TransactionEntity();
						txEntity.setCreatedAt(LocalDate.now());
						txEntity.setAccountNo(savedInvest.getAccountNo());
						txEntity.setTransactionNo(generateTransactionNo());
						txEntity.setTrType(CustomerConstant.TX_TYPE_DEPOSIT);
						txEntity.setAmount(amount);
						txEntity.setCustPassportNo(save.getPassportNo());
						txEntity.setCustDelegName(save.getDelegFullName());
						txEntity.setCustDelegPassportNo(save.getDelegPassport());
						txEntity.setCustFirstName(save.getFirstName());
						txEntity.setCustMiddleName(save.getMiddleName());
						txEntity.setCustLastName(save.getLastName());
						txEntity.setNotes(CustomerConstant.TX_DESC_DEPOSIT_INVEST);
						txEntity.setStatus(CustomerConstant.TR_STATUS_COMPLETE);
						txEntity.setProcessId(savedInvest.getId());
						txEntity.setApprovedAt(LocalDate.now());
						txEntity.setReceivedAt(LocalDate.now());
						txEntity.setCustDelegImage(save.getDelegImage());
						TransactionEntity trEntity = txRepo.save(txEntity);
						result = "تم تشغيل أرباحك بنجاح";
						// send email to customer
						try {
							sendEmail.sendInvestMoneyRequestComplete(customerEntity);
						} catch (MessagingException | IOException | TemplateException e) {
							e.printStackTrace();
						}
					} else {
						result = "فشل إضافة الرصيد ، الرجاء المحاولة مرة أخرى";
					}
				} else {
					result = "المبلغ أكبر من الرصيد المتاح";
				}
			} else {
				result = "عفوا ، ليس لديك أرباح لتشغيلها";
			}
		} else {
			result = "الرجاء تسجيل الدخول لتتمكن من إكمال العملية";
		}
		return result;
	}

	private Long generateAccountNo() {
		Random rand = new Random();
		Long num = (long) (rand.nextInt(9000000) + 1000000);
		Optional<CustomerEntity> findByAccountNo = custRepo.findByAccountNo(num);
		if (findByAccountNo.isPresent() && findByAccountNo.get() != null) {
			return num + 1;
		}
		return num;
	}

	private Long generateTransactionNo() {
		Random rand = new Random();
		Long num = (long) (rand.nextInt(9000) + 1000);
		Optional<TransactionEntity> findbByTrNo = txRepo.findByTransactionNo(num);
		if (findbByTrNo.isPresent() && findbByTrNo.get() != null) {
			return num + 1;
		}
		return num;
	}

	@Override
	public String activateCustomer(Integer custId) {
		Optional<CustomerEntity> findById = custRepo.findById(custId);
		if (findById.isPresent()) {
			findById.get().setAccountStatus(AppConstant.ACCOUNT_STATUS_ACTIVE);
			custRepo.save(findById.get());
			return AppConstant.ACCOUNT_ACTIVATED;
		}
		return AppConstant.ACCOUNT_NOT_ACTIVATED;
	}

	@Override
	public String depositMoney(DepositeRequest drequest) {
		String result = "فشلت عملية الإيداع ، الرجاء ادخال مبلغ صحيح";
		CustomerEntity cust = custRepo.findByAccountNo(drequest.getAccountNo()).get();
		if (cust != null) {
			if (drequest != null && drequest.getAmount() != null && drequest.getAmount() > 0) {
				DepositeRequestEntity entity = new DepositeRequestEntity();
				drequest.setCreatedAt(LocalDate.now());
				drequest.setStatus(CustomerConstant.TR_STATUS_PENDDING);
				BeanUtils.copyProperties(drequest, entity);
				DepositeRequestEntity save = depRepo.save(entity);
				if (save != null) {
					// register transaction details
					TransactionEntity txEntity = new TransactionEntity();
					txEntity.setCreatedAt(LocalDate.now());
					txEntity.setAccountNo(save.getAccountNo());
					txEntity.setTransactionNo(generateTransactionNo());
					txEntity.setTrType(CustomerConstant.TX_TYPE_DEPOSIT);
					txEntity.setAmount(drequest.getAmount());
					txEntity.setCustPassportNo(cust.getPassportNo());
					txEntity.setCustDelegName(cust.getDelegFullName());
					txEntity.setCustDelegPassportNo(cust.getDelegPassport());
					txEntity.setCustFirstName(cust.getFirstName());
					txEntity.setCustMiddleName(cust.getMiddleName());
					txEntity.setCustLastName(cust.getLastName());
					txEntity.setNotes(CustomerConstant.TX_DESC_DEPOSIT_DP);
					txEntity.setStatus(CustomerConstant.TR_STATUS_PENDDING);
					txEntity.setProcessId(save.getId());
					txEntity.setCustImage(cust.getImage());
					txEntity.setCustId(cust.getId());
					txEntity.setCustDelegImage(cust.getDelegImage());
					TransactionEntity trEntity = txRepo.save(txEntity);
					result = "تم إرسال طلب الإيداع لمدير المنظومة";
					// send email
					try {
						sendEmail.sendDepositRequest(cust);
					} catch (MessagingException | IOException | TemplateException e) {
						e.printStackTrace();
					}
				} else {
					result = "فشلت عملية الإيداع ، الرجاء ادخال مبلغ صحيح";
				}
			} else {
				result = "فشلت عملية الإيداع ، الرجاء ادخال مبلغ صحيح";
			}
		} else {
			result = "فشلت عملية الإيداع ، الرجاء ادخال مبلغ صحيح";
		}

		return result;
	}

	@Override
	public String transferMoney(TransferRequest trequest) {
		String result = "الحقول فارغة ، يجب ملئها";
		if (trequest != null && trequest.getAmount() != null && trequest.getAmount() > 0
				&& trequest.getReceAccountNo() != null && trequest.getReceName() != null) {
			// check whether customer has balance or not
			CustomerEntity customerEntity = custRepo.findByAccountNo(trequest.getAccountNo()).get();
			if (customerEntity != null) {
				if (customerEntity.getBalanceToWithdraw() != null && customerEntity.getBalanceToWithdraw() > 0) {
					if (customerEntity.getBalanceToWithdraw() > trequest.getAmount()) {
						// check whether the receiver account no correct or not
						Optional<CustomerEntity> receEntity = custRepo.findByAccountNo(trequest.getReceAccountNo());
						if (receEntity != null && receEntity.isPresent()) {
							// set the transaction details
							trequest.setStatus(CustomerConstant.TR_STATUS_PENDDING);
							trequest.setCreatedAt(LocalDate.now());
							TransferEntity entity = new TransferEntity();
							BeanUtils.copyProperties(trequest, entity);
							TransferEntity save = trRepo.save(entity);
							if (save != null) {
								// update customer balance
								customerEntity.setBalanceToWithdraw(
										customerEntity.getBalanceToWithdraw() - trequest.getAmount());
								custRepo.save(customerEntity);
								// register public transaction details
								TransactionEntity txEntity = new TransactionEntity();
								txEntity.setCreatedAt(LocalDate.now());
								txEntity.setAccountNo(save.getAccountNo());
								txEntity.setTransactionNo(generateTransactionNo());
								txEntity.setTrType(CustomerConstant.TX_TYPE_TRANSFER);
								txEntity.setAmount(trequest.getAmount());
								txEntity.setCustPassportNo(customerEntity.getPassportNo());
								txEntity.setCustDelegName(customerEntity.getDelegFullName());
								txEntity.setCustDelegPassportNo(customerEntity.getDelegPassport());
								txEntity.setCustFirstName(customerEntity.getFirstName());
								txEntity.setCustMiddleName(customerEntity.getMiddleName());
								txEntity.setCustLastName(customerEntity.getLastName());
								txEntity.setNotes(CustomerConstant.TX_DESC_TRANSFER);
								txEntity.setStatus(CustomerConstant.TR_STATUS_PENDDING);
								txEntity.setProcessId(save.getId());
								TransactionEntity trEntity = txRepo.save(txEntity);
								result = CustomerConstant.TRANSFER_SUCCESS;
							} else {
								result = "فشل إرسال طلب التحويل ، حاول مرة أخرى";
							}

						} else {
							result = "رقم حساب المرسل إليه غير صحيح";
						}
					} else {
						result = "القيمة المرسلة أكبر من الرصيد المتاح";
					}
				} else {
					result = "ليس لديك رصيد كافي للتحويل";
				}
			} else {
				result = "الرجاء تسجيل الدخول لتتمكن من إتمام العملية";
			}
		} else {
			result = "الحقول فارغة ، يجب ملئها";
		}
		return result;
	}

	@Override
	public List<TransferEntity> getTransfersByAccountNo(Long accountNo) {
		System.out.println("accountNo = " + accountNo);
		Optional<List<TransferEntity>> list = trRepo.findByAccountNo(accountNo);
		if (list != null && list.isPresent() && !list.get().isEmpty()) {
			return list.get();
		} else {
			System.out.println("it is null");
			return null;
		}
	}

	@Override
	public List<WithdrawRequestEntity> getWithdrawByAccountNo(Long accountNo) {
		System.out.println("accountNo = " + accountNo);
		Optional<List<WithdrawRequestEntity>> list = wrRepo.findByAccountNo(accountNo);
		if (list != null && list.isPresent() && !list.get().isEmpty()) {
			return list.get();
		} else {
			System.out.println("it is null");
			return null;
		}
	}

	@Override
	public List<TransactionEntity> transactions(Long accountNo) {
		List<TransactionEntity> list = txRepo.transactions(accountNo).get();
		list.forEach(t -> {
			System.out.println(t);
		});
		return list;
	}

	@Override
	public String findImageById(Integer custId) {
		return custRepo.findById(custId).get().getImage();
	}

	@Override
	public String resetPassword(ResetPassword restPassword) {
		String encryptedPassword = null;
		try {
			encryptedPassword = PasswordEncryptionUsingAES.encryptPassword(restPassword.getPassword());
		} catch (Exception e) {
			e.printStackTrace();
		}
		CustomerEntity customerEntity = custRepo.findById(restPassword.getId()).get();
		customerEntity.setPassword(encryptedPassword);
		CustomerEntity save = custRepo.save(customerEntity);
		if (save != null) {
			return "تم تغير كلمة السر بنجاح";
		} else {
			return "فشل تغير كلمة السر";
		}
	}

	@Override
	public String forgotPassword(String email) {
		String result = "لبريد الإلكتروني غير موجود";
		// get customer's details by his email
		CustomerEntity customerEntity = custRepo.findByEmail(email).get();
		if (customerEntity != null) {
			// send email to customer containing link to reset his password
			try {
				sendEmail.sendForgotPasswordEmail(customerEntity);
			} catch (MessagingException | IOException | TemplateException e) {
				e.printStackTrace();
			}
			result = "تم إرسال رابط تعيين كلمة المرور لبريدك الإلكتروني";
		}
		return result;
	}

	@Override
	public String findDelegImageById(Integer custId) {
		return custRepo.findById(custId).get().getDelegImage();
	}

	@Override
	public String cancelTx(Integer id) {
		String result = "";
		// get the transaction based on the id
		TransactionEntity txEntity = txRepo.findByProcessId(id);
		// get the tx type
		if (txEntity.getTrType().equals("deposit")) {
			// get the specific tx
			DepositeRequestEntity dpEntity = depRepo.findById(id).get();
			dpEntity.setStatus(CustomerConstant.TR_STATUS_CANCELED);
			txEntity.setStatus(CustomerConstant.TR_STATUS_CANCELED);
			// save
			depRepo.save(dpEntity);
		} else if (txEntity.getTrType().equals("withdraw")) {
			// get the specific tx
			WithdrawRequestEntity wdEntity = wrRepo.findById(id).get();
			wdEntity.setStatus(CustomerConstant.TR_STATUS_CANCELED);
			txEntity.setStatus(CustomerConstant.TR_STATUS_CANCELED);
			// save
			wrRepo.save(wdEntity);
			// update customer balance
			CustomerEntity customerEntity = custRepo.findByAccountNo(txEntity.getAccountNo()).get();
			if (customerEntity.getBalanceToWithdraw() != null && customerEntity.getBalanceToWithdraw() > 0) {
				customerEntity.setBalanceToWithdraw((customerEntity.getBalanceToWithdraw()) + txEntity.getAmount());
				customerEntity.setSuspendedBalance((customerEntity.getSuspendedBalance()) - txEntity.getAmount());
			} else {
				customerEntity.setBalanceToWithdraw(txEntity.getAmount());
				customerEntity.setSuspendedBalance((customerEntity.getSuspendedBalance()) - txEntity.getAmount());
			}
			CustomerEntity save = custRepo.save(customerEntity);
			if (save != null) {
				result = "تم إلغاء العملية";
			} else {
				result = "فشل إلغاء العملية";
			}

		} else {
			// get the specific tx
			TransferEntity trEntity = trRepo.findById(id).get();
			trEntity.setStatus(CustomerConstant.TR_STATUS_CANCELED);
			txEntity.setStatus(CustomerConstant.TR_STATUS_CANCELED);
			// save
			trRepo.save(trEntity);
			// update customer balance
			CustomerEntity customerEntity = custRepo.findByAccountNo(txEntity.getAccountNo()).get();
			if (customerEntity.getBalanceToWithdraw() != null && customerEntity.getBalanceToWithdraw() > 0) {
				customerEntity.setBalanceToWithdraw((customerEntity.getBalanceToWithdraw()) + txEntity.getAmount());
				customerEntity.setSuspendedBalance((customerEntity.getSuspendedBalance()) - txEntity.getAmount());
			} else {
				customerEntity.setBalanceToWithdraw(txEntity.getAmount());
				customerEntity.setSuspendedBalance((customerEntity.getSuspendedBalance()) - txEntity.getAmount());
			}
			CustomerEntity save = custRepo.save(customerEntity);
			if (save != null) {
				result = "تم إلغاء العملية";
			} else {
				result = "فشل إلغاء العملية";
			}
		}
		TransactionEntity save = txRepo.save(txEntity);
		if (save != null) {
			result = "تم إلغاء العملية";
		} else {
			result = "فشل إلغاء العملية";
		}
		return result;
	}

	@Override
	public List<DepositeRequestEntity> getDepositByAccountNo(Long accountNo) {
		System.out.println("accountNo = " + accountNo);
		Optional<List<DepositeRequestEntity>> list = depRepo.findByAccountNo(accountNo);
		if (list != null && list.isPresent() && !list.get().isEmpty()) {
			return list.get();
		} else {
			System.out.println("it is null");
			return null;
		}
	}

	@Override
	public String sendWithdrawEmail(Integer id) {
		String result = "الرجاء تسجيل الدخول لتتمكن من إجراء العملية";
		TransactionEntity tx = txRepo.findByProcessId(id);
		if (tx != null) {
			// get customer email
			CustomerEntity customerEntity = custRepo.findByAccountNo(tx.getAccountNo()).get();
			if (customerEntity != null) {
				// send email to customer contains tx no
				try {
					sendEmail.sendWithdrawRequestEmail(customerEntity, tx.getWithdrawDate().toString(),
							tx.getTransactionNo().toString());
					result = "تم إرسال رمز السحب إلى بريدك الإلكتروني";
				} catch (MessagingException | IOException | TemplateException e) {
					e.printStackTrace();
					result = "فشل الإرسال ، حاول مرة أخرى";
				}
			} else {
				result = "الرجاء تسجيل الدخول لتتمكن من إجراء العملية";
			}
		} else {
			result = "الرجاء تسجيل الدخول لتتمكن من إجراء العملية";
		}
		return result;
	}

	@Override
	public boolean isEmailAvailable(String email) {
		System.out.println("entering to service method");
		Optional<CustomerEntity> findByEmail = custRepo.findByEmail(email);
		if (findByEmail != null && findByEmail.isPresent()) {
			System.out.println("it is not present");
			return true;
		} else {
			System.out.println("it is present");
			return false;
		}
	}

	@Override
	public String findCustPassportImageById(Integer custId) {
		return custRepo.findById(custId).get().getCustPassImage();
	}

	@Override
	public List<TransactionEntity> getWithdrawsTx(Long accountNo) {
		List<TransactionEntity> findWithdrawsTx = txRepo.findWithdrawsTx(accountNo);
		return findWithdrawsTx;
	}
}