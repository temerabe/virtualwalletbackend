package org.dello.reg.customer.repository;

import java.util.List;
import java.util.Optional;

import org.dello.reg.customer.constant.CustomerConstant;
import org.dello.reg.customer.entity.TransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {
	Optional<TransactionEntity> findByTransactionNo(Long transactionNo);

	Optional<List<TransactionEntity>> findByAccountNo(Long acount);

	@Query(value = "from TransactionEntity where accountNo =:accountNo and status = 'complete' ")
	Optional<List<TransactionEntity>> transactions(@Param(value = "accountNo") Long acount);

	@Query(value = "from TransactionEntity where transactionNo =:trNo and status = 'reviewed' ")
	TransactionEntity completeTransaction(@Param(value = "trNo") Long acount);

	TransactionEntity findByProcessId(Integer processId);

	@Query(value = "from TransactionEntity where accountNo =:accountNo and trType = 'withdraw'")
	List<TransactionEntity> findWithdrawsTx(@Param(value = "accountNo") Long accountNo);
}
