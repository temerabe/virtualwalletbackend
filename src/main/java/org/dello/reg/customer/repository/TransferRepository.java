package org.dello.reg.customer.repository;

import java.util.List;
import java.util.Optional;

import javax.persistence.Entity;

import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.TransactionEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TransferRepository extends JpaRepository<TransferEntity, Integer> {
	Optional<List<TransferEntity>> findByAccountNo(Long accountNo);

	@Query(value = "from TransferEntity where status = 'pendding' ")
	Optional<List<TransferEntity>> viewPendingTransferRequest();

	@Query(value = "select count(cust) from TransferEntity cust where cust.status = 'complete' ")
	Long getLastCompleteTransferCount();

	@Query(value = "select count(cust) from TransferEntity cust where cust.status = 'pendding' ")
	Long getPenddingTransferCounts();
}
