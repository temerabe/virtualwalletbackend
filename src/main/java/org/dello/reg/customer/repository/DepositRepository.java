package org.dello.reg.customer.repository;

import java.util.List;
import java.util.Optional;

import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DepositRepository extends JpaRepository<DepositeRequestEntity, Integer> {
	@Query(value = "from DepositeRequestEntity where status = 'pendding' ")
	Optional<List<DepositeRequestEntity>> viewPendingDepositRequest();

	@Query(value = "select count(cust) from DepositeRequestEntity cust where cust.status = 'pendding' ")
	Long getPenddingDepositsCount();

	Optional<List<DepositeRequestEntity>> findByAccountNo(Long accountNo);
}
