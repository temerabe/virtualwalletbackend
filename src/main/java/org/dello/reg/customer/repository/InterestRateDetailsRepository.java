package org.dello.reg.customer.repository;

import java.util.List;

import org.dello.reg.customer.entity.InterestRateDetailsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InterestRateDetailsRepository extends JpaRepository<InterestRateDetailsEntity, Integer> {
	@Query(value = "from InterestRateDetailsEntity where isAdded = false ")
	List<InterestRateDetailsEntity> findAllPenddingInterest();

}
