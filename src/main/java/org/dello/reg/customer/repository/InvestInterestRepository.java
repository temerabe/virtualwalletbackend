package org.dello.reg.customer.repository;

import java.util.List;
import java.util.Optional;

import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.InvestInterestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface InvestInterestRepository extends JpaRepository<InvestInterestEntity, Integer> {
	@Query(value = "from InvestInterestEntity where status = 'pendding' ")
	Optional<List<InvestInterestEntity>> viewPendingInvestInterestRequest();
}
