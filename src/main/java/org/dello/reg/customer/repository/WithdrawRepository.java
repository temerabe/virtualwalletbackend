package org.dello.reg.customer.repository;

import java.util.List;
import java.util.Optional;

import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface WithdrawRepository extends JpaRepository<WithdrawRequestEntity, Integer> {
	Optional<List<WithdrawRequestEntity>> findByAccountNo(Long accountNo);

	@Query(value = "from WithdrawRequestEntity where status = 'pendding' ")
	List<WithdrawRequestEntity> viewPendingWithdraRequest();

	@Query(value = "select count(cust) from WithdrawRequestEntity cust where cust.status = 'pendding' ")
	Long getPenddingWithdrawsCount();

	@Query(value = "select count(cust) from WithdrawRequestEntity cust where cust.status = 'complete' ")
	Long getLastCompleteWithdrawsCount();

	@Query(value = "select amount from WithdrawRequestEntity")
	Optional<List<Float>> getAmount();

}
