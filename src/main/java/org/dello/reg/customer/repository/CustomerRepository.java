package org.dello.reg.customer.repository;

import java.util.List;
import java.util.Optional;

import org.dello.reg.customer.entity.CustomerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<CustomerEntity, Integer> {
	Optional<CustomerEntity> findByAccountNo(Long accountNo);

	@Query(value = "from CustomerEntity where email=:email and password=:password")
	Optional<CustomerEntity> doLogin(@Param(value = "email") String email, @Param(value = "password") String password);

	@Query(value = "from CustomerEntity where accountStatus = 'inactive' and isProfileUpdated = true")
	List<CustomerEntity> penddinRegistration();

	@Query(value = "from CustomerEntity where accountStatus = 'active'")
	List<CustomerEntity> getActiveCustomer();

	@Query(value = "select count(cust) from CustomerEntity cust where cust.accountStatus = 'inactive' ")
	Long getInActiveCustomerCount();

	Optional<CustomerEntity> findByEmail(String email);

	@Query(value = "from CustomerEntity where totalBalance > 0 and totalBalance != null")
	List<CustomerEntity> findCustomerForInterest();
}
