package org.dello.reg.customer.repository;

import org.dello.reg.customer.entity.CustomerBalanceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerBalanceRepository extends JpaRepository<CustomerBalanceEntity, Integer> {

}
