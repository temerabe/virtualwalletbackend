package org.dello.reg.customer.model;

import lombok.Data;
@Data
public class CustomerResponse {
	private Integer id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String password;
	private String email;
	private Long accountNo;
	private byte[] Image;
	private String accountStatus;
	private Long passportNo;
	private Long phoneNo;
	private String delegFullName;
	private String role;
	private Long delegPassport;
	private String delegImage;
	private float totalBalance;
	private float availableBalance;
	private float balanceToWithdraw;
	private float suspendedBalance;
	public CustomerResponse() {
		// no body
	}
	public CustomerResponse(Integer id, String firstName, String middleName, String lastName, String password,
			String email, Long accountNo, byte[] image, String accountStatus, Long passportNo, Long phoneNo,
			String delegFullName, String role, Long delegPassport, String delegImage, float totalBalance,
			float availableBalance, float balanceToWithdraw, float suspendedBalance) {
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.password = password;
		this.email = email;
		this.accountNo = accountNo;
		Image = image;
		this.accountStatus = accountStatus;
		this.passportNo = passportNo;
		this.phoneNo = phoneNo;
		this.delegFullName = delegFullName;
		this.role = role;
		this.delegPassport = delegPassport;
		this.delegImage = delegImage;
		this.totalBalance = totalBalance;
		this.availableBalance = availableBalance;
		this.balanceToWithdraw = balanceToWithdraw;
		this.suspendedBalance = suspendedBalance;
	}
}
