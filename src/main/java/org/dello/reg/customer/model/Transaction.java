package org.dello.reg.customer.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class Transaction {
	private Integer id;
	private Long transactionNo;
	private float amount;
	private String custFirstName;
	private String custMiddleName;
	private String custLastName;
	private String trType;
	private String notes;
	private Long accountNo;
	private String custPassportNo;
	private String custDelegName;
	private String custDelegImage;
	private String custDelegPassportNo;
	private String status;
	private LocalDate createdAt;
	private LocalDate approvedAt;
	private LocalDate receivedAt;

	public Transaction() {
		// no body
	}

	public Transaction(Integer id, Long transactionNo, Float amount, String custFirstName, String custMiddleName,
			String custLastName, String trType, String notes, Long accountNo, String custPassportNo,
			String custDelegName, String custDelegImage, String custDelegPassportNo, String status, LocalDate createdAt,
			LocalDate approvedAt, LocalDate receivedAt) {
		this.id = id;
		this.transactionNo = transactionNo;
		this.amount = amount;
		this.custFirstName = custFirstName;
		this.custMiddleName = custMiddleName;
		this.custLastName = custLastName;
		this.trType = trType;
		this.notes = notes;
		this.accountNo = accountNo;
		this.custPassportNo = custPassportNo;
		this.custDelegName = custDelegName;
		this.custDelegImage = custDelegImage;
		this.custDelegPassportNo = custDelegPassportNo;
		this.status = status;
		this.createdAt = createdAt;
		this.approvedAt = approvedAt;
		this.receivedAt = receivedAt;
	}

}
