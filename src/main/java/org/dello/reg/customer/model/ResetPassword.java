package org.dello.reg.customer.model;

import lombok.Data;

@Data
public class ResetPassword {
	private Integer id;
	private String password;
	private String email;

	public ResetPassword() {
		// no body
	}

	public ResetPassword(Integer id, String password, String email) {
		this.id = id;
		this.password = password;
		this.email = email;
	}

}
