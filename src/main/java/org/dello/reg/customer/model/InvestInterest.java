package org.dello.reg.customer.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class InvestInterest {
	private Integer id;
	private String name;
	private Long accountNo;
	private float amount;
	private String status;
	private String notes;
	private LocalDate createdAt;
	private LocalDate approvedAt;
	private LocalDate receivedAt;

	public InvestInterest() {
		// no body
	}

	public InvestInterest(Integer id, String name, Long accountNo, Float amount, String status, String notes,
			LocalDate createdAt, LocalDate approvedAt, LocalDate receivedAt) {
		this.id = id;
		this.name = name;
		this.accountNo = accountNo;
		this.amount = amount;
		this.status = status;
		this.notes = notes;
		this.createdAt = createdAt;
		this.approvedAt = approvedAt;
		this.receivedAt = receivedAt;
	}
}
