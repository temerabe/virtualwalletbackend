package org.dello.reg.customer.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class WithdrawRequest {
	private Integer id;
	private Long accountNo;
	private Float amount;
	private LocalDate createdAt;
	// by application
	private float actualAmount;
	private String notes;
	// by application
	private String status;
	// by application
	private LocalDate withdrawDate;
	private String withdrawWay;

	public WithdrawRequest() {
		// no body
	}

	public WithdrawRequest(Integer id, Long accountNo, Float amount, LocalDate createdAt, Float actualAmount,
			String notes, String status, LocalDate withdrawDate, String withdrawWay) {
		this.id = id;
		this.accountNo = accountNo;
		this.amount = amount;
		this.createdAt = createdAt;
		this.actualAmount = actualAmount;
		this.notes = notes;
		this.status = status;
		this.withdrawDate = withdrawDate;
		this.withdrawWay = withdrawWay;
	}
}
