package org.dello.reg.customer.model;

import java.time.LocalDate;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class Customer {
	private Integer id;
	// @NotNull(message = "الإسم الأول يجب أن لا يكون فارغا")
	private String firstName;
	// @NotNull(message = "الإسم الأوسط يجب أن لا يكون فارغا")
	private String middleName;
	// @NotNull(message = "الإسم الأخير يجب أن لا يكون فارغا")
	private String lastName;
	// @NotNull(message = "كلمة المرور يجب أن لا تكون فارغة")
	// @Size(max = 15, min = 6, message = "أدنى حد لكلمة المرور 8 والأعلى 15")
	private String password;
	// @NotNull(message = " البريد الإلكتروني يجب أن لا يكون فارغة")
	// @Email
	private String email;
	private Long accountNo;
	private String accountStatus;
	private MultipartFile Image;
	private String passportNo;
	private String phoneNo;
	private String delegFullName;
	private String delegPassport;
	private MultipartFile delegtImage;
	private Float totalBalance;
	private Float availableBalance;
	private Float balanceToWithdraw;
	private Float suspendedBalance;
	private String role;
	private float interestRate;
	private float intersetRatePlus;
	private boolean isProfileUpdated;
	private LocalDate createdAt;

	public Customer() {
		// no body
	}

	public Customer(Integer id, String firstName, String middleName, String lastName, String password, String email,
			Long accountNo, String accountStatus, MultipartFile image, String passportNo, String phoneNo,
			String delegFullName, String delegPassport, MultipartFile delegtImage, float totalBalance,
			float availableBalance, float balanceToWithdraw, float suspendedBalance, String role, float interestRate,
			float intersetRatePlus, Boolean isProfileUpdated, LocalDate createdAt) {
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.password = password;
		this.email = email;
		this.accountNo = accountNo;
		this.accountStatus = accountStatus;
		Image = image;
		this.passportNo = passportNo;
		this.phoneNo = phoneNo;
		this.delegFullName = delegFullName;
		this.delegPassport = delegPassport;
		this.delegtImage = delegtImage;
		this.totalBalance = totalBalance;
		this.availableBalance = availableBalance;
		this.balanceToWithdraw = balanceToWithdraw;
		this.suspendedBalance = suspendedBalance;
		this.role = role;
		this.interestRate = interestRate;
		this.intersetRatePlus = intersetRatePlus;
		this.isProfileUpdated = isProfileUpdated;
		this.createdAt = createdAt;
	}
}