package org.dello.reg.customer.model;

import java.time.LocalDate;

import javax.persistence.Column;

import lombok.Data;

@Data
public class TransferRequest {

	private Integer id;
	private Long accountNo;
	private Float amount;
	private String status;
	private String receName;
	private Long receAccountNo;
	private Long recePassportNo;
	private String transferWay;
	private LocalDate createdAt;
	private LocalDate approvedAt;
	private LocalDate receivedAt;

	public TransferRequest() {
		// no body
	}

	public TransferRequest(Integer id, Long accountNo, Float amount, String status, String receName, Long receAccountNo,
			Long recePassportNo, String transferWay, LocalDate createdAt, LocalDate approvedAt, LocalDate receivedAt) {
		this.id = id;
		this.accountNo = accountNo;
		this.amount = amount;
		this.status = status;
		this.receName = receName;
		this.receAccountNo = receAccountNo;
		this.recePassportNo = recePassportNo;
		this.transferWay = transferWay;
		this.createdAt = createdAt;
		this.approvedAt = approvedAt;
		this.receivedAt = receivedAt;
	}

}
