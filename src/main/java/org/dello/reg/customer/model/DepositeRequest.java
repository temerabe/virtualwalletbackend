package org.dello.reg.customer.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class DepositeRequest {
	private Integer id;
	private String name;
	private Long accountNo;
	private Float amount;
	private String status;
	private LocalDate createdAt;
	private LocalDate approvedAt;
	private LocalDate receivedAt;

	public DepositeRequest() {
		// no body
	}

	public DepositeRequest(Integer id, String name, Long accountNo, Float amount, String status, LocalDate createdAt,
			LocalDate approvedAt, LocalDate receivedAt) {
		this.id = id;
		this.name = name;
		this.accountNo = accountNo;
		this.amount = amount;
		this.status = status;
		this.createdAt = createdAt;
		this.approvedAt = approvedAt;
		this.receivedAt = receivedAt;
	}
}
