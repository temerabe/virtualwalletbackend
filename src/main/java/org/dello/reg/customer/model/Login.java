package org.dello.reg.customer.model;

import lombok.Data;

@Data
public class Login {
	private String email;
	private String password;
	private String role;
	private String token;
	public Login() {
		// no body
	}
	public Login(String email, String password, String role, String token) {
		this.email = email;
		this.password = password;
		this.role = role;
		this.token = token;
	}
}
