package org.dello.reg.customer.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class InterestRateDetails {
	private Integer id;
	private String firstName;
	private String middleName;
	private String lastName;
	private String accountNo;
	private Integer customerId;
	private float interestRate;
	private float interestRatePlus;
	private float amountWithInterestRate;
	private float amountWithInterestRatePlus;
	private float custBalance;
	private float custInteresBalance;
	private boolean isAdded;
	private LocalDate createdAt;
	private LocalDate approvedAt;
	private String intrType;

	public InterestRateDetails() {
		// no body
	}

	public InterestRateDetails(Integer id, String firstName, String middleName, String lastName, String accountNo,
			Integer customerId, Float interestRate, Float interestRatePlus, Float amountWithInterestRate,
			Float amountWithInterestRatePlus, Float custBalance, Float custInteresBalance, Boolean isAdded,
			LocalDate createdAt, LocalDate approvedAt, String intrType) {
		this.id = id;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.accountNo = accountNo;
		this.customerId = customerId;
		this.interestRate = interestRate;
		this.interestRatePlus = interestRatePlus;
		this.amountWithInterestRate = amountWithInterestRate;
		this.amountWithInterestRatePlus = amountWithInterestRatePlus;
		this.custBalance = custBalance;
		this.custInteresBalance = custInteresBalance;
		this.isAdded = isAdded;
		this.createdAt = createdAt;
		this.approvedAt = approvedAt;
		this.intrType = intrType;
	}

}
