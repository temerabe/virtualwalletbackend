package org.dello.reg.customer.model;

import java.time.LocalDate;

import lombok.Data;

@Data
public class CustomerBalance {
	private Integer id;
	private Long accountNo;
	private Float amount;
	private LocalDate startedAt;
	private Integer workingDays;
	private Boolean isFromBegining;
	private String proccessType;
	private String notes;

	public CustomerBalance() {
		// no body
	}
}
