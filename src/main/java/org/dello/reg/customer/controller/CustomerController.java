package org.dello.reg.customer.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import org.dello.reg.customer.entity.CustomerEntity;
import org.dello.reg.customer.entity.DepositeRequestEntity;
import org.dello.reg.customer.entity.TransactionEntity;
import org.dello.reg.customer.entity.TransferEntity;
import org.dello.reg.customer.entity.WithdrawRequestEntity;
import org.dello.reg.customer.model.Customer;
import org.dello.reg.customer.model.DepositeRequest;
import org.dello.reg.customer.model.ResetPassword;
import org.dello.reg.customer.model.TransferRequest;
import org.dello.reg.customer.model.WithdrawRequest;
import org.dello.reg.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class CustomerController {
	@Autowired
	private CustomerService custService;

	@GetMapping("/")
	public String hello() {
		return "hello";
	}

	/**
	 * This method is used to add one customer to the database.
	 * 
	 * @param cust the customer entity that has to be added
	 * @return the customer entity that has been added to the database or null
	 *         something went wrong.
	 */
	@PostMapping("/customers")
	public CustomerEntity createCustomer(@RequestBody Customer cust) {
		return custService.registerCustomer(cust);

	}

	/**
	 * This method is used to activate customer account after registering his
	 * details in the application.
	 * 
	 * @param custId the customer id send by the activation link.
	 * @return message indicating that whether customer's account activated or not.
	 */
	@GetMapping("/customers/email")
	public boolean isEmailAvailable(@RequestParam(name = "email") String email) {
		System.out.println("entering to controller");
		return custService.isEmailAvailable(email);
	}
	@GetMapping("/customers/wrs/{accountNo}")
	public List<TransactionEntity> getWithdrawsRequest(@PathVariable(name="accountNo")Long accountNo) {
		System.out.println("acount no = " + accountNo);
		return custService.getWithdrawsTx(accountNo);
	}

	@GetMapping("/customers/activate/{custId}")
	public String doActivation(@PathVariable(name = "custId") Integer custId) {
		return custService.activateCustomer(custId);

	}

	@GetMapping("/customer/image/{custId}")
	public byte[] getImage(@PathVariable(name = "custId") Integer custId) {
		String imagePath = custService.findImageById(custId);
		Path path = Paths.get(imagePath);
		byte[] readAllBytes = null;
		try {
			readAllBytes = Files.readAllBytes(path);
			// byte[] encode = Base64.getEncoder().encode(readAllBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readAllBytes;
	}

	@GetMapping("/customer/delegImage/{custId}")
	public byte[] getDelegImage(@PathVariable(name = "custId") Integer custId) {
		String imagePath = custService.findDelegImageById(custId);
		Path path = Paths.get(imagePath);
		byte[] readAllBytes = null;
		try {
			readAllBytes = Files.readAllBytes(path);
			// byte[] encode = Base64.getEncoder().encode(readAllBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readAllBytes;
	}

	@GetMapping("/customer/custPassportImage/{custId}")
	public byte[] getCustPassportImage(@PathVariable(name = "custId") Integer custId) {
		String imagePath = custService.findCustPassportImageById(custId);
		Path path = Paths.get(imagePath);
		byte[] readAllBytes = null;
		try {
			readAllBytes = Files.readAllBytes(path);
			// byte[] encode = Base64.getEncoder().encode(readAllBytes);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return readAllBytes;
	}

	@PostMapping("/customers/tx/wr/sendmail")
	public String reSendWithdrawEmail(@RequestParam(name = "id") Integer id) {
		return custService.sendWithdrawEmail(id);
	}

	@PostMapping("/customers/forgotpassword")
	public String forgotPassword(@RequestParam(name = "email", required = false) String email) {
		String forgotPassword = "الرجاء إدخال البريد الإلكتروني";
		if (!email.equals("") || email != null) {
			forgotPassword = custService.forgotPassword(email);
		}
		return forgotPassword;
	}

	@PostMapping("/customers/resetpassword")
	public String resetPassword(@RequestBody ResetPassword resetPassword) {
		return custService.resetPassword(resetPassword);
	}

	/**
	 * This method is used to get one customer based on his id.
	 * 
	 * @param custId the customer id
	 * @return the entity of the customer containing all his details.
	 */
	@GetMapping(value = "/customers/{custId}", produces = { MediaType.APPLICATION_JSON_VALUE,
			MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.TEXT_PLAIN_VALUE })
	public CustomerEntity getCustomer(@PathVariable("custId") Integer custId) {
		return custService.getCustomer(custId);

	}

	/**
	 * This method is used to update customer's profile and uploading his images.
	 * 
	 * @param customer
	 * @param file
	 * @return
	 */
	@PutMapping(value = "/customers", consumes = { "multipart/form-data", "application/json" })
	public CustomerEntity createCustomerPending(@RequestParam(name = "cust") String customer,
			@RequestParam(name = "file", required = false) MultipartFile file,
			@RequestParam(name = "custPassImage", required = false) MultipartFile custPassImage,
			@RequestParam(name = "custDelegImage", required = false) MultipartFile custDelegImage) {
		ObjectMapper mapper = new ObjectMapper();
		Customer cust = null;
		boolean custImageFlag = false;
		boolean custPassImageFlag = false;
		boolean custDelegImageFlag = false;
		try {
			cust = mapper.readValue(customer, Customer.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		CustomerEntity customerEntity = custService.getCustomer(cust.getId());
		// upload the images to server file system
		String custImage = null;
		String delegImage = null;
		String passImage = null;
		if (file != null) {
			if (!file.isEmpty()) {
				try {
					custImage = custService.uploadFile(file, customerEntity.getId());
					custImageFlag = true;
					System.out.println("image path = " + custImage);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (custPassImage != null) {
			if (!custPassImage.isEmpty()) {
				try {
					passImage = custService.uploadFile(custPassImage, customerEntity.getId());
					custPassImageFlag = true;
					System.out.println("image path = " + passImage);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		if (custDelegImage != null) {
			if (!custDelegImage.isEmpty()) {
				try {
					delegImage = custService.uploadFile(custDelegImage, customerEntity.getId());
					custDelegImageFlag = true;
					System.out.println("image path = " + passImage);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// set the paths of images to the entity to save
		customerEntity.setImage(custImage);
		customerEntity.setPassportNo(cust.getPassportNo());
		customerEntity.setPhoneNo(cust.getPhoneNo());
		customerEntity.setDelegFullName(cust.getDelegFullName());
		customerEntity.setDelegPassport(cust.getDelegPassport());
		customerEntity.setIsProfileUpdated(true);
		customerEntity.setDelegImage(delegImage);
		customerEntity.setCustPassImage(passImage);
		CustomerEntity updateCustomer = custService.updateCustomer(customerEntity);
		return updateCustomer;
	}

	@PutMapping(value = "customers/tx/cancel")
	public String cancelTransaction(@RequestParam(name = "id") Integer id) {
		if (id != null && !id.equals("")) {
			return custService.cancelTx(id);
		} else {
			return "الحقل فارغ";
		}

	}

	/**
	 * This method is used to withdraw all customer's interest.
	 * 
	 * @param wrequest the withdraw request
	 * @return the withdraw response back to customer.
	 */
	@PutMapping("/customers/withdraw")
	public String withdraw(@RequestBody WithdrawRequest wrequest) {
		String result = "الحقول فارغة ، الرجاء ملئها";
		if (wrequest != null && wrequest.getAccountNo() != null && wrequest.getAmount() != null) {
			result = custService.withdrawMoney(wrequest);
		}
		return result;

	}

	/*
	 * @PutMapping("/customers/invest") public String invsetMoney(@RequestBody
	 * CustomerEntity customer) { String investMoney =
	 * custService.investMoney(customer); if (investMoney == null) {
	 * System.out.println("from controller , it is null"); } return investMoney; }
	 */
	@PutMapping("/customers/invest")
	public String invsetMoney(@RequestParam(name = "id") Integer id, @RequestParam(name = "amount") Float amount) {
		String investMoney = custService.investMoney(id, amount);
		if (investMoney == null) {
			System.out.println("from controller , it is null");
		}
		return investMoney;
	}

	@PutMapping("/customers/deposit")
	public String deposit(@RequestBody DepositeRequest drequest) {
		return custService.depositMoney(drequest);
	}

	@PutMapping("/customers/transfer")
	public String transfer(@RequestBody TransferRequest trequest) {
		return custService.transferMoney(trequest);

	}

	@GetMapping("/customers/transfers/{accountNo}")
	public List<TransferEntity> viewTransfers(@PathVariable(name = "accountNo") Long accountNo) {
		return custService.getTransfersByAccountNo(accountNo);
	}

	@GetMapping("/customers/withdraw/{accountNo}")
	public List<WithdrawRequestEntity> viewWithdraw(@PathVariable(name = "accountNo") Long accountNo) {
		return custService.getWithdrawByAccountNo(accountNo);
	}

	@GetMapping("/customers/deposit/{accountNo}")
	public List<DepositeRequestEntity> viewDeposits(@PathVariable(name = "accountNo") Long accountNo) {
		return custService.getDepositByAccountNo(accountNo);
	}

	@GetMapping("/customers/transactions/{accountNo}")
	public List<TransactionEntity> getTransaction(@PathVariable(name = "accountNo") Long accountNo) {
		return custService.transactions(accountNo);
	}
}