package org.dello.reg.customer.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
public class TransferEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUST_SEQ")
	@SequenceGenerator(name = "CUST_SEQ", sequenceName = "CUSTOMER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private Integer id;
	@Column(name = "ACCOUNT_NO", length = 10)
	private Long accountNo;
	@Column(name = "AMOUNT", length = 10)
	private float amount;
	@Column(name = "STATUS", length = 15)
	private String status;
	@Column(name = "RECE_NAME", length = 150)
	private String receName;
	@Column(name = "RECE_ACCOUNT_NO", length = 10)
	private Long receAccountNo;
	@Column(name = "RECE_PASSPORT_NO", length = 10)
	private Long recePassportNo;
	@Column(name = "TRANSFER_WAY", length = 15)
	private String transferWay;
	@Column(name = "CREATED_AT")
	private LocalDate createdAt;
	@Column(name = "APPROVED_AT")
	private LocalDate approvedAt;
	@Column(name = "RECEIVED_AT")
	private LocalDate receivedAt;
}