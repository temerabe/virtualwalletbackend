package org.dello.reg.customer.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;

@Entity
@Data
public class InterestRateDetailsEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUST_SEQ")
	@SequenceGenerator(name = "CUST_SEQ", sequenceName = "CUSTOMER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private Integer id;
	@Column(name = "FIRST_NAME", length = 15)
	private String firstName;
	@Column(name = "MIDDLE_NAME", length = 15)
	private String middleName;
	@Column(name = "LAST_NAME", length = 15)
	private String lastName;
	@Column(name = "ACCOUNT_NO", length = 10)
	private Long accountNo;
	@Column(name = "CUSTOMER_ID", length = 10)
	private Integer customerId;
	@Column(name = "INTEREST_RATE", length = 10)
	private Float interestRate;
	@Column(name = "INTEREST_RATE_PLUS", length = 10)
	private Float interestRatePlus;
	@Column(name = "AMOUNT_WITH_INTEREST_RATE")
	private Float amountWithInterestRate;
	@Column(name = "AMOUNT_WITH_INTEREST_RATE_PLUS")
	private Float amountWithInterestRatePlus;
	@Column(name = "CUST_BALANCE")
	private Float custBalance;
	@Column(name = "CUST_INTEREST_BALANCE")
	private Float custInteresBalance;
	@Column(name = "IS_ADDED")
	private Boolean isAdded;
	@Column(name = "CREATED_AT")
	private LocalDate createdAt;
	@Column(name = "APPROVED_AT")
	private LocalDate approvedAt;
	@Column(name = "INTR_TYPE",length=15)
	private String intrType;
}