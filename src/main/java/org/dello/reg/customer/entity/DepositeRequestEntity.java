package org.dello.reg.customer.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class DepositeRequestEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUST_SEQ")
	@SequenceGenerator(name = "CUST_SEQ", sequenceName = "CUSTOMER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private Integer id;
	@Column(name = "NAME", length = 150)
	private String name;
	@Column(name = "ACCOUNT_NO", length = 10)
	private Long accountNo;
	@Column(name = "AMOUNT", length = 15)
	private float amount;
	@Column(name = "STATUS", length = 15)
	private String status;
	@Column(name = "CREATED_AT")
	private LocalDate createdAt;
	@Column(name = "APPROVED_AT")
	private LocalDate approvedAt;
	@Column(name = "RECEIVED_AT")
	private LocalDate receivedAt;

}
