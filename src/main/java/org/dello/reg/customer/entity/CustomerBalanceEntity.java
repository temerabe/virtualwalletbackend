package org.dello.reg.customer.entity;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import lombok.Data;

@Entity
@Data
public class CustomerBalanceEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUST_SEQ")
	@SequenceGenerator(name = "CUST_SEQ", sequenceName = "CUSTOMER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private Integer id;
	@Column(name = "account_no", length = 15)
	private Long accountNo;
	@Column(name = "amount")
	private float amount;
	@Column(name = "STARTED_AT")
	private LocalDate startedAt;
	@Column(name = "PROCESS_TYPE", length = 15)
	private String proccessType;
	@Column(name = "NOTES")
	private String notes;
	@Column(name = "WORKING_DAYS")
	private Integer workingDays;
	@Column(name = "IS_FROM_BEGINING")
	private boolean isFromBegining;
}