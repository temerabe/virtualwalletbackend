package org.dello.reg.customer.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class CustomerEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUST_SEQ")
	@SequenceGenerator(name = "CUST_SEQ", sequenceName = "CUSTOMER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private Integer id;
	@Column(name = "FIRST_NAME", length = 15)
	private String firstName;
	@Column(name = "MIDDLE_NAME", length = 15)
	private String middleName;
	@Column(name = "LAST_NAME", length = 15)
	private String lastName;
	@Column(name = "PASSWORD", length = 250)
	private String password;
	@Column(name = "EMAIL", length = 100, unique = true)
	private String email;
	@Column(name = "ACCOUNT_NO", length = 100)
	private Long accountNo;
	@Column(name = "IMAGE", length = 250)
	private String Image;
	@Column(name = "CUST_PASS_IMAGE")
	private String custPassImage;
	@Column(name = "DELEG_IMAGE")
	private String delegImage;
	@Column(name = "ACCOUNT_STATUS", length = 30)
	private String accountStatus;
	@Column(name = "PASSPORT_NO", length = 15)
	private String passportNo;
	@Column(name = "PHONE_NO", length = 15)
	private String phoneNo;
	@Column(name = "DELEG_NAME", length = 70)
	private String delegFullName;
	@Column(name = "ROLE", length = 30)
	private String role;
	@Column(name = "DELEG_PASSPORT", length = 15)
	private String delegPassport;
	@Column(name = "TOTAL_BALANCE")
	private Float totalBalance;
	@Column(name = "AVAILABLE_BALANCE")
	private Float availableBalance;
	@Column(name = "BALANCE_TO_WITHDRAW")
	private Float balanceToWithdraw;
	@Column(name = "SUSPENDED_BALANCE")
	private Float suspendedBalance;
	@Column(name = "INTEREST_RATE")
	private Float interestRate;
	@Column(name = "INTEREST_RATE_PLUS")
	private Float intersetRatePlus;
	@Column(name = "IS_PROFILE_UPDATED")
	private Boolean isProfileUpdated;
	@Column(name = "CREATED_AT")
	private LocalDate createdAt;
	@Column(name = "WORKING_DAYS")
	private Integer workingDays;
	@Column(name = "IS_FROM_BEGINING")
	private Boolean isFromBegining;
}