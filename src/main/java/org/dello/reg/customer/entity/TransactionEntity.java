package org.dello.reg.customer.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table
@Data
public class TransactionEntity {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "CUST_SEQ")
	@SequenceGenerator(name = "CUST_SEQ", sequenceName = "CUSTOMER_SEQ", initialValue = 101)
	@Column(name = "ID", length = 10)
	private Integer id;
	@Column(name = "TX_NO", length = 10)
	private Long transactionNo;
	@Column(name = "AMOUNT")
	private Float amount;
	@Column(name = "CUST_FIRST_NAME", length = 150)
	private String custFirstName;
	@Column(name = "CUST_MIDDLE_NAME", length = 150)
	private String custMiddleName;
	@Column(name = "CUST_LAST_NAME", length = 150)
	private String custLastName;
	@Column(name = "TR_TYPE", length = 15)
	private String trType;
	@Column(name = "NOTES", length = 255)
	private String notes;
	@Column(name = "ACCOUNT_NO", length = 10)
	private Long accountNo;
	@Column(name = "CUST_PASSPORT_NO", length = 10)
	private String custPassportNo;
	@Column(name = "CUST_DELEG_NAME", length = 150)
	private String custDelegName;
	@Column(name = "CUST_DELEG_IMAGE", length = 150)
	private String custDelegImage;
	@Column(name = "CUST_DELEG_PASSPORT_NO", length = 10)
	private String custDelegPassportNo;
	@Column(name = "STATUS", length = 15)
	private String status;
	@Column(name = "CREATED_AT")
	private LocalDate createdAt;
	@Column(name = "APPROVED_AT")
	private LocalDate approvedAt;
	@Column(name = "RECEIVED_AT")
	private LocalDate receivedAt;
	@Column(name = "PROCESS_ID")
	private Integer processId;
	@Column(name = "CUST_IMAGE")
	private String custImage;
	@Column(name = "CUST_ID")
	private Integer custId;
	@Column(name = "STARTED_AT")
	private LocalDate startedAt;
	@Column(name = "WITHDRAW_DATE")
	private String withdrawDate;
	@Column(name = "CUSTOMER_BALANCE")
	private Float customerBalance;
}