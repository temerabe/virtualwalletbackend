package org.dello.reg.customer.constant;

public class CustomerConstant {
	public static final String WITHDRAW_WAY = "نقدا";
	public static final String WITHDRAW_SUCCESS = "تم إرسال طلب السحب لمدير المنظومة";
	public static final String WITHDRAW_FAILED = "ليس لديك رصيد قابل للسحب";
	public static final String WITHDRAW_PENDING = "pending";
	public static final String WITHDRAW_PENDING_AR = "تمت العملية ، في إنتظار المراجعة";
	public static final String WITHDRAW_REVIEW = "review";
	public static final String WITHDRAW_REVIEW_AR = "تمت العملية ، في إنتظار السحب";
	public static final String WITHDRAW_COMPLETE = "complete";
	public static final String WITHDRAW_COMPLETE_AR = "تمت عملية السحب";
	public static final String DEPOSIT_SUCCESS = "تم إرسال طلب الإيداع لمدير المنظومة";
	public static final String TRANSFER_SUCCESS = "تم إرسال طلب التحويل لمدير المنظومة";
	public static final String TR_STATUS_PENDDING = "pendding";
	public static final String TR_STATUS_APPROVED = "reviewed";
	public static final String TR_STATUS_COMPLETE = "complete";
	public static final String TR_STATUS_CANCELED = "canceled";
	public static final String TX_TYPE_DEPOSIT = "deposit";
	public static final String TX_TYPE_WITHDRAW = "withdraw";
	public static final String TX_TYPE_TRANSFER = "transfer";
	public static final String TX_DESC_WITHDRAW = "سحب مبلغ مالي بناء على طلب العميل";
	public static final String TX_DESC_DEPOSIT = "إيداع أرباح الشهر - ";
	public static final String TX_DESC_DEPOSIT_DP = "إيداع مبلغ مالي عن طريق مكتبنا";
	public static final String TX_DESC_DEPOSIT_INVEST = "تشغيل الارباح لشهر - ";
	public static final String TX_DESC_TRANSFER = "تحويل مبلغ مالي بناء على طلب العميل";
}
