package org.dello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@SpringBootApplication
public class VirsualWallectAppApplication implements WebMvcConfigurer {

	public static void main(String[] args) {
		SpringApplication.run(VirsualWallectAppApplication.class, args);
	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*").allowedMethods("*");
	}

	@Bean
	public AmazonS3Client getAmazonS3() {
		AWSCredentials credentials = new BasicAWSCredentials("AKIAVHPTRFCH4NGA3LHN",
				"YxfnnEtreXVM9d4HYnQYn1zTmF6bvcgEL7buZsMI");
		AmazonS3Client s3client = (AmazonS3Client) AmazonS3ClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(Regions.US_EAST_2).build();
		return s3client;
	}
}